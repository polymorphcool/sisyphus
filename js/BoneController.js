/*
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      

 libre video game bazaar

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of sysiphus project
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2018 polymorph.cool
 All rights reserved. 
 
 MIT License

 Permission is hereby granted, free of charge, to any person obtaining a copy of this
 software and associated documentation files (the "Software"), to deal in the Software 
 without restriction, including without limitation the rights to use, copy, modify, merge,
 publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 to whom the Software is furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED *AS IS*, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.
 
 ___________________________________( ^3^)_____________________________________
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
*/

function BoneController() {

	this.bone = undefined;
	this.parent = undefined;
	this.local_scale = undefined;

	this.inherit_scale = false;

	this.init  = function( bone, parent ) {
		this.bone = bone;
		this.parent = parent;
		this.local_scale = new THREE.Vector3(1,1,1);
	};

	this.to_string = function() {
		var out = this.bone.name;
		if ( this.parent === undefined ) {
			out += ", no parent";
		} else {
			out += " < " + this.parent.name;
		}
		return out;
	};

	this.set = function( vec3 ) {
		this.local_scale = vec3.clone();
	};

	this.length = function( y ) {
		if ( y < 1e-5 ) {
			y = 1e-5;
		}
		this.local_scale.y = y;
	};

	this.radii = function( x, z = undefined ) {
		if ( x < 1e-5 ) {
			x = 1e-5;
		}
		if ( z === undefined ) {
			z = x;
		} else if ( z < 1e-5 ) {
			z = 1e-5;
		}
		this.local_scale.x = x;
		this.local_scale.z = z;
	};

	this.get_inverse_scale = function() {
		var sc = this.local_scale;
		return new THREE.Vector3(1/sc.x,1/sc.y,1/sc.z);
	}

	this.update = function() {
		var new_scale = this.local_scale;
		if ( this.parent !== undefined && !this.inherit_scale ) {
			var pisc = this.parent.get_inverse_scale();
			new_scale = new THREE.Vector3(
				this.local_scale.x * pisc.x,
				this.local_scale.y * pisc.y,
				this.local_scale.z * pisc.z
				);
		}
		this.bone.scale.set( new_scale.x, new_scale.y, new_scale.z );
	};

};