/*
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      

 libre video game bazaar

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of sysiphus project
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2018 polymorph.cool
 All rights reserved. 
 
 MIT License

 Permission is hereby granted, free of charge, to any person obtaining a copy of this
 software and associated documentation files (the "Software"), to deal in the Software 
 without restriction, including without limitation the rights to use, copy, modify, merge,
 publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 to whom the Software is furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED *AS IS*, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.
 
 ___________________________________( ^3^)_____________________________________
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
*/

if ( ! Detector.webgl ) Detector.addGetWebGLMessage();

// *********************************
// ************ CONFIG *************
// *********************************

var config_sysiphus_avatar = new SysiphusAvatarConfig();
config_sysiphus_avatar.mesh = "Body_Idle";
config_sysiphus_avatar.skeleton_root = "Armature_breathing";
config_sysiphus_avatar.path_model = 'models/geraldine.dae';
config_sysiphus_avatar.path_animations = ['models/geraldine_jump_back.dae'];
config_sysiphus_avatar.name_animations = ["jump back"];
config_sysiphus_avatar.name_default_animation = "jump back";
config_sysiphus_avatar.path_deforms = ['presets/deform_test.json','presets/deform_test2.json'];

// *********************************
// ************ GLOBALS ************
// *********************************

var stats_enabled = true;

var container, stats, clock, controls;
var camera, scene, renderer;

var gridHelper = undefined;

var SCENE_LOADED = false;

var avatar = undefined;
var cush = undefined;

// debug objects
var frame_count = 0;

var ui_settings = undefined;

var texture_canvas = undefined;
var texture_context = undefined;
var texture_texture = undefined;
var texture_cellnum = 32;

var collider = undefined;

// *********************************
// *********** FUNCTIONS ***********
// *********************************

init();

animate();

function init() {

	container = document.getElementById( 'container' );
	camera = new THREE.PerspectiveCamera( 25, window.innerWidth / window.innerHeight, 1, 10000 );
	camera.position.set( 5, 2.5, 5 );
	scene = new THREE.Scene();
	clock = new THREE.Clock();

	gridHelper = new THREE.GridHelper( 10, 20 );
	scene.add( gridHelper );

	// lightning
	var ambientLight = new THREE.AmbientLight( 0xffffff, 0.2 );
	scene.add( ambientLight );
	var directionalLight = new THREE.DirectionalLight( 0xffffff, 0.8 );
	directionalLight.position.set( 1, 1, 1 );
	scene.add( directionalLight );

	// renderer
	renderer = new THREE.WebGLRenderer( { antialias: true } );
	renderer.setPixelRatio( window.devicePixelRatio );
	renderer.setSize( window.innerWidth, window.innerHeight );
	renderer.setClearColor( new THREE.Color( 0xaaaaaa ), 1 );
	container.appendChild( renderer.domElement );

	// camera controls
	controls = new THREE.OrbitControls( camera, renderer.domElement );
	controls.target.set( 0, 2, 0 );
	controls.update();

	// stats
	stats = undefined;
	if ( stats_enabled ) {
		stats = new Stats();
		container.appendChild( stats.dom );
	}

	// debug
	var geometry;
	var material;
	var wireframe;

	// avatar
	avatar = new SysiphusAvatar();
	avatar.ready_callbak = on_avatar_ready;
	avatar.configure( config_sysiphus_avatar );
	avatar.load( scene );

	gridHelper.position.set( 0, 1, 0 );

	// physics
	CushinWorld.init();
	CushinWorld.create_ground( 0, 0.4, 0, 0.05 );
	cush = new Cushin();
	cush.init( 
		0, 2, -1.2, // pos
		0.5, // radius
		20, 20, // def
		15, // mass
		190, // pressure 
		0.05 // margin  
		);

	collider = CushinWorld.createParalellepiped( 
		1, 0.2, 1, // pos
		0, // mass
		new THREE.Vector3(0,2.5,0), 
		new THREE.Quaternion(),
		new THREE.MeshPhongMaterial( { color: 0xFF0000 } ) );

	// events
	window.addEventListener( 'resize', onWindowResize, false );

}

function on_avatar_ready() {

	avatar.move( 0, 1, 0 );
	avatar.obj3d_mesh.visible = false;

}

function onWindowResize() {

	camera.aspect = window.innerWidth / window.innerHeight;
	camera.updateProjectionMatrix();
	renderer.setSize( window.innerWidth, window.innerHeight );

}

function animate() {

	requestAnimationFrame( animate );
	render();
	if ( stats_enabled ) {
		stats.update();
	}

}

function render() {

	var delta = clock.getDelta();

	if ( avatar.ready === true ) {

		avatar.update( delta );

		
		collider.userData.physicsBody.setActivationState( 4 );
		var bttransform = collider.userData.physicsBody.getWorldTransform();
		var transform = new Ammo.btTransform();
		var ms = collider.userData.physicsBody.getMotionState();
		ms.getWorldTransform( transform );
		var root_bone = avatar.bones[ "mixamorig_Hips" ];

		if ( frame_count < 50 ) {
			console.log(collider.userData.physicsBody);
			console.log(ms);
			console.log(
				bttransform.getOrigin().x() + ", " +
				bttransform.getOrigin().y() + ", " +
				bttransform.getOrigin().z()
				);
			console.log(
				bttransform.getOrigin().x() + ", " +
				bttransform.getOrigin().y() + ", " +
				bttransform.getOrigin().z()
				);
			console.log(
				root_bone.world_position.x + ", " +
				root_bone.world_position.y + ", " +
				root_bone.world_position.z
				);
		}
		
		transform.setIdentity();
		transform.setOrigin( new Ammo.btVector3( 
				root_bone.world_position.x,
				root_bone.world_position.y,
				root_bone.world_position.z
				) );
		transform.setRotation( new Ammo.btQuaternion(  
				root_bone.world_quaternion.x,
				root_bone.world_quaternion.y,
				root_bone.world_quaternion.z
				) );
		// ms.setWorldTransform( transform );
		// var ms = new Ammo.btDefaultMotionState( bttransform );
		// collider.userData.physicsBody.setWorldTransform(bttransform);
		// collider.userData.physicsBody.setMotionState(ms);
		// collider.userData.physicsBody.forceActivationState();
		// collider.userData.physicsBody.activate();

		CushinWorld.update( delta );

	}

	renderer.render( scene, camera );

	frame_count++;

}

function print( e ) {}