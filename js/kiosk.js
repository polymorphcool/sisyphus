// black squares of ground will be pure black
var GROUND_PURE_BLACK = true;
// menu closes automatically after x seconds
var MENU_TIMEOUT = 180;
// enables controls of the camera by just moving the mouse
var CONTROL_MOUSE_MOTION = true;