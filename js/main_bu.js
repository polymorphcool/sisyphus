/*
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      

 libre video game bazaar

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of sysiphus project
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2018 polymorph.cool
 All rights reserved. 
 
 MIT License

 Permission is hereby granted, free of charge, to any person obtaining a copy of this
 software and associated documentation files (the "Software"), to deal in the Software 
 without restriction, including without limitation the rights to use, copy, modify, merge,
 publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 to whom the Software is furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED *AS IS*, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.
 
 ___________________________________( ^3^)_____________________________________
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
*/

if ( ! Detector.webgl ) Detector.addGetWebGLMessage();

// *********************************
// ************ CONFIG *************
// *********************************

var model_url = 'models/geraldine.dae';
// WARNING: models and name have the inverse order!
var model_animations = ['models/geraldine_default.dae','models/geraldine_sleeping.dae','models/geraldine_sleeping_back.dae'];
var model_animation_names = ["idle","default","sleeping side","sleeping back"];
var model_animation_default = "idle"; // will be activated after loading

var deforms = ['presets/deform_test.json','presets/deform_test2.json'];

// *********************************
// ************ GLOBALS ************
// *********************************

var stats_enabled = true;

var container, stats, clock, controls;
var camera, scene, renderer;

var mixer = undefined;
var animations = undefined;
var avatar = undefined;
var gridHelper = undefined;

var info_animations = undefined;
var info_animations_body = undefined;

var current_main_anim = undefined;
var request_anim_name = undefined;

// becomes true only after all assests are loaded and initailised
var ANIM_LOADED = false;
var PRESETS_LOADED = false;
var SCENE_LOADED = false;

/* list of bones
 * key: bone name
 * value: BoneController
 */
var bone_mgr = undefined;

// animation

var angle0 = 0;

// debug objects

var debug_cube = undefined;

// *********************************
// *********** FUNCTIONS ***********
// *********************************

init();
animate();

function init() {
	container = document.getElementById( 'container' );
	camera = new THREE.PerspectiveCamera( 25, window.innerWidth / window.innerHeight, 1, 10000 );
	camera.position.set( 5, 2.5, 5 );
	scene = new THREE.Scene();
	clock = new THREE.Clock();
	var loader = new THREE.ColladaLoader();
	print( "loading " + model_url );
	loader.load( model_url, function ( collada ) {
		// console.log( loader );
		animations = collada.animations;
		avatar = collada.scene;
		// var action = mixer.clipAction( animations[ 0 ] ).play();
		//action.setEffectiveWeight( 0.5 );
		scene.add( avatar );
		avatar.traverse( function ( child ) {
			if ( child.name == "Armature_breathing" ) {

				console.log( "found #Armature_breathing!!!" );
				console.log( child );

				bone_mgr = new BoneManager();
				bone_mgr.init( child );
				print( bone_mgr.to_string() );

				bone_mgr.loading_callback( on_scale_presets_loading );
				bone_mgr.loaded_callback( on_scale_presets_loaded );
				bone_mgr.load_presets( deforms );
				// console.log( bone_mgr );

				skeleton = new THREE.SkeletonHelper( child );
				skeleton.visible = true;
				// console.log( skeleton );
				scene.add( skeleton );

			}
		} );
		// animation list loading
		on_animation_loaded();
	} );
	gridHelper = new THREE.GridHelper( 10, 20 );
	scene.add( gridHelper );
	// lightning
	var ambientLight = new THREE.AmbientLight( 0xffffff, 0.2 );
	scene.add( ambientLight );
	var directionalLight = new THREE.DirectionalLight( 0xffffff, 0.8 );
	directionalLight.position.set( 1, 1, 1 );
	scene.add( directionalLight );
	// renderer
	renderer = new THREE.WebGLRenderer( { antialias: true } );
	renderer.setPixelRatio( window.devicePixelRatio );
	renderer.setSize( window.innerWidth, window.innerHeight );
	container.appendChild( renderer.domElement );
	// camera controls
	controls = new THREE.OrbitControls( camera, renderer.domElement );
	controls.target.set( 0, 2, 0 );
	controls.update();
	// stats
	stats = undefined;
	if ( stats_enabled ) {
		stats = new Stats();
		container.appendChild( stats.dom );
	}
	// debug
	var geometry = new THREE.BoxGeometry( 0.3, 0.3, 0.3 );
	var material = new THREE.MeshBasicMaterial( { vertexColors: THREE.FaceColors, overdraw: 0.5 } );
	debug_cube = new THREE.Mesh( geometry, material );
	scene.add( debug_cube );
	// events
	window.addEventListener( 'resize', onWindowResize, false );
}

// animation list loader, consuming model_animations
function on_animation_loaded() {
	// sequential loading of collada with animations
	if ( model_animations.length > 0 ) {
		var al = model_animations.shift();
		console.log( "loading " + al );
		print( "loading " + al );
		var animloader = new THREE.ColladaLoader();
		animloader.load( al, function ( collada ) {
			var alist = collada.animations;
			for ( var i = 0; i < animations[0].tracks.length; ++i ) {
				alist[ 0 ].tracks[i].name = animations[0].tracks[i].name;
			}
			animations.push( alist[ 0 ] );
			// renaming all keys
			on_animation_loaded();
		} );
		return;
	}
	// loading is over
	on_loading_complete();
}

function on_loading_complete() {
	// renaming animation sequentially
	for ( var i = 0; i < animations.length; ++i ) {
		if ( i < model_animation_names.length ) {
			animations[i].name = model_animation_names[i];
		} else {
			animations[i].name = "anim_" + i;
		}
	}
	// booting the mixer
	mixer = new THREE.AnimationMixer( avatar );
	mixer.addEventListener( 'loop', function( e ) { mixer_loop_event(e); } );
	// console dump
	// console.log( "#################\n ANIMATIONS\n" );
	// console.log( animations );
	// console.log( "#################\n AVATAR\n" );
	// console.log( avatar );
	// console.log( "#################\n MIXER\n" );
	// console.log( mixer );
	// setting all weights to 0
	for ( var i = 0; i < animations.length; ++i ) {
		var track = mixer.clipAction( animations[i] );
		track.setEffectiveWeight( 0 );
		track.enabled = true;
		track.just_looped = false;
	}
	// enabing the first animation
	request_anim_name = model_animation_default;
	// moving the grid and the model up for the camera
	gridHelper.position.set( 0, 1, 0 );
	avatar.position.set( 0, 1, 0 );
	
	ANIM_LOADED = true;
	print( "all animations are loaded" );
	// trying to start the scene
	start_scene();
}

function on_scale_presets_loading( path ) {
	print( "loading preset " + path );
}

function on_scale_presets_loaded() {
	PRESETS_LOADED = true;
	console.log( bone_mgr.presets );
	print( "all presets are loaded" );
	// trying to start the scene
	start_scene();
}

function start_scene() {
	if ( PRESETS_LOADED && ANIM_LOADED ) {
		// populate UI
		populate_info();
		// let's rock!
		SCENE_LOADED = true;
	}
}

function number_display( num ) {
	var str = "" + Math.floor( num * 100 ) / 100;
	var n = str.indexOf(".");
	if ( n == -1 ) {
		return str + ".00";
	} else if  ( n == str.length - 2 ) {
		return str + "0";
	}
	return str;
}

function populate_info() {
	info_animations = document.getElementById( 'info_animations' );
	info_animations_body = document.getElementById( 'info_animations_body' );
	var row;
	var cell;
	for ( var i = 0; i < animations.length; ++i ) {
		var j = 0;
		var weight = mixer.clipAction( animations[i] ).getEffectiveWeight();
		// adding a row
		row = info_animations_body.insertRow(info_animations_body.rows.length);
		row.id = "info_" + animations[i].name;
		// adding cells
		// name
		cell = row.insertCell(j); j++;
		cell.className = "name first";
		cell.appendChild(document.createTextNode( animations[i].name ));
		// duration
		cell = row.insertCell(j); j++;
		cell.className = "duration";
		cell.appendChild(document.createTextNode( number_display( animations[i].duration ) ));
		// time
		cell = row.insertCell(j); j++;
		cell.id = "info_time_" + animations[i].name;
		cell.className = "time";
		cell.appendChild(document.createTextNode( number_display( mixer.clipAction( animations[i] ).time ) ));
		// speed
		cell = row.insertCell(j); j++;
		cell.id = "info_speed_" + animations[i].name;
		cell.className = "speed";
		cell.appendChild(document.createTextNode( number_display( mixer.clipAction( animations[i] ).timeScale ) ));
		// weight
		cell = row.insertCell(j); j++;
		cell.id = "info_weight_" + animations[i].name;
		cell.className = "weight";
		cell.appendChild(document.createTextNode( number_display( weight ) ));
		// loop display
		cell = row.insertCell(j); j++;
		cell.id = "info_looped_" + animations[i].name;
		cell.className = "looped";
		var checkbox = document.createElement('div');
		checkbox.className = "looped_checkbox";
		checkbox.id = "info_looped_checkbox_" + animations[i].name;
		cell.appendChild( checkbox );
		// checkbox
		cell = row.insertCell(j); j++;
		cell.id = "info_radio_" + animations[i].name;
		cell.className = "radio last";
		var checkbox = document.createElement('input');
		checkbox.type = "radio";
		checkbox.name = "radio_animation_selector";
		checkbox.id = "radio_" + animations[i].name;
		checkbox.value = animations[i].name;
		checkbox.className = "radio_animation";
		checkbox.addEventListener( "click", function(){ request_anim_name = this.value; } );
		if ( animations[i].name == request_anim_name ) {
			checkbox.checked = true;
		}
		cell.appendChild( checkbox ) ;
	}
	info_scales = document.getElementById( 'info_scale_presets' );
	info_scales_body = document.getElementById( 'info_scale_presets_body' );
	for ( var name in bone_mgr.presets ) {
		var j = 0;
		// adding a row
		row = info_scales_body.insertRow(info_scales_body.rows.length);
		row.id = "info_scale_presets_" + name;
		// adding cells
		// name
		cell = row.insertCell(j); j++;
		cell.className = "name first";
		cell.appendChild(document.createTextNode( name ));
		// checkbox
		cell = row.insertCell(j); j++;
		cell.id = "info_scale_presets_" + name;
		cell.className = "radio last";
		var checkbox = document.createElement('input');
		checkbox.type = "radio";
		checkbox.name = "radio_scale_presets_selector";
		checkbox.id = "radio_" + name;
		checkbox.value = name;
		checkbox.className = "radio_animation";
		checkbox.addEventListener( "click", function(){ bone_mgr.goto_preset( this.value ); } );
		if ( name == "__UNIT__" ) {
			checkbox.checked = true;
		}
		cell.appendChild( checkbox ) ;
	}
}

function switch_animation() {
	if ( mixer === undefined || request_anim_name === undefined ) {
		return;
	}
	var prev_anim = current_main_anim;
	for ( var i = 0; i < animations.length; ++i ) {
		if ( animations[i].name == request_anim_name ) {
			current_main_anim = animations[i];
		}
	}
	request_anim_name = undefined;
	if ( prev_anim == current_main_anim ) {
		console.log( "SAME ANIM! " + current_main_anim.name );
		return;
	}
	for ( var i = 0; i < animations.length; ++i ) {
		var track = mixer.clipAction( animations[i] );
		if ( current_main_anim == animations[i] ) {
			track.enabled = true;
			track.stopFading();
			track.play();
			// track.setEffectiveWeight( 1 );
		} 
		// else if ( track.getEffectiveWeight() == 1 ) {
		// 	track.fadeOut( 1 );
		// }
	}
}

function check_animations( delta ) {
	if ( mixer !== undefined ) {
		if ( request_anim_name !== undefined ) {
			switch_animation( request_anim_name );
			request_anim_name = undefined;
		}
		var track = mixer.clipAction( current_main_anim );
		var w = track.getEffectiveWeight();
		if ( w < 1 ) {
			w += delta;
			if ( w > 1 ) {
				w = 1;
			}
			track.setEffectiveWeight( w );
		}
		for ( var i = 0; i < animations.length; ++i ) {
			var track = mixer.clipAction( animations[i] );
			track.just_looped = false;
			if ( current_main_anim == animations[i] ) {
				continue;
			}
			var w = track.getEffectiveWeight();
			if ( w > 0 ) {
				w -= delta;
				if ( w <= 0 ) {
					w = 0;
					track.stop();
				}
				track.setEffectiveWeight( w );
			}
		}
	}
}

function mixer_loop_event( e ) {
	// console.log( e );
	for ( var i = 0; i < animations.length; ++i ) {
		if ( e.action._clip.name == animations[i].name ) {
			mixer.clipAction( animations[i] ).just_looped = true;
			return;
		}
	}
}

function update_info() {
	if ( mixer !== undefined ) {
		for ( var i = 0; i < animations.length; ++i ) {
			var aname = animations[i].name;
			var track = mixer.clipAction( animations[i] );
			document.getElementById( "info_weight_" + aname ).innerHTML = 
				number_display( track.getEffectiveWeight() );
			document.getElementById( "info_speed_" + aname ).innerHTML = 
				number_display( track.getEffectiveTimeScale() );
			document.getElementById( "info_time_" + aname ).innerHTML = 
				number_display( track.time );
			var cb = document.getElementById( "info_looped_checkbox_" + aname );
			if ( track.just_looped && cb.className != "looped_checkbox checked" ) {
				cb.className = "looped_checkbox checked";
			} else if ( cb.className == "looped_checkbox checked" ) {
				cb.className = "looped_checkbox normal";
			}
		}
	}
}

function onWindowResize() {
	camera.aspect = window.innerWidth / window.innerHeight;
	camera.updateProjectionMatrix();
	renderer.setSize( window.innerWidth, window.innerHeight );
}

function animate() {
	requestAnimationFrame( animate );
	render();
	if ( stats_enabled ) {
		stats.update();
	}
}

function render() {

	var delta = clock.getDelta();

	check_animations( delta );

	if ( SCENE_LOADED ) {
		if ( mixer !== undefined ) {
			mixer.update( delta );
		}
		angle0 += delta * 2;
		// bone_mgr.length( "mixamorig_LeftUpLeg", 1 + ( 1 + Math.sin( angle0 ) ) * 0.5 );
		// bone_mgr.radii( "mixamorig_LeftUpLeg", 1 + ( 1 + Math.sin( angle0 ) ) * 0.5 );
		bone_mgr.update( delta );
		//bone_mgr.track_to( "mixamorig_Spine2", camera, 0.3 );
		bone_mgr.track_to( "mixamorig_Neck", camera, 0.5 );
		bone_mgr.track_to( "mixamorig_Head", camera, 0.8 );
		// bone_mgr.track_to( "mixamorig_HeadTop_End", camera.position );
		// debug_json( camera.position );
		// debug_json( bone_mgr.ctrls["mixamorig_LeftLeg"].local_scale );
	}
	renderer.render( scene, camera );
	
	if ( SCENE_LOADED ) {
		update_info();
	}

}

function print( e ) {
	document.getElementById("debugger").innerHTML += e;
	document.getElementById("debugger").innerHTML += "<br/>";
}

function debug( e ) {
	document.getElementById("debugger").innerHTML = e;
}

function debug_json( e ) {
	document.getElementById("debugger").innerHTML = JSON.stringify(e, null, 4);
}