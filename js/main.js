/*

 _________ ____	.-. _________/ ____ .-. ____
 __|__	(_)_/(_)(	 )____<		\|		(	 )	(_)
								 `-'								 `-'

 libre video game bazaar

 ____________________________________	?	 ____________________________________
																		 (._.)


 This file is part of sysiphus project
 For the latest info, see http://polymorph.cool/

 Copyright (c) 2018 polymorph.cool
 All rights reserved.

 MIT License

 Permission is hereby granted, free of charge, to any person obtaining a copy of this
 software and associated documentation files (the "Software"), to deal in the Software
 without restriction, including without limitation the rights to use, copy, modify, merge,
 publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED *AS IS*, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.

 ___________________________________( ^3^)_____________________________________

 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)

*/

if ( ! Detector.webgl ) {
	Detector.addGetWebGLMessage();
}

// *********************************
// ************ CONFIG *************
// *********************************

{
	
	// DO NOT FORGET TO TURN THIS OFF FOR PROD!
	var skip_splash = false;
	var remote_timestamp_server = "server/json/timestamp.json";
	var remote_gallery_server = "server/json/gallery.json";

	var SHADOW_ENABLED = true;
	var SHADOW_MAP_WIDTH = 2048, SHADOW_MAP_HEIGHT = 2048;

	// GROUND
	var config_ground = new GroundConfig();
	config_ground.rows = 30;
	config_ground.columns = 30;
	config_ground.size.set( 0.35, 0, 0.35 );
	config_ground.use_cubes = false;
	config_ground.ready_callbak = on_ground_ready;

	// NEW GERALDINE
	var config_sysiphus_avatar = new SysiphusAvatarConfig();
	config_sysiphus_avatar.mesh = "body";
	config_sysiphus_avatar.skeleton_root = "geraldine";
	config_sysiphus_avatar.path_model = 'models/geraldine2.dae';
	config_sysiphus_avatar.path_animations = [
		'models/geraldine2_idle.dae',
		'models/geraldine2_arms_crossed.dae',
		'models/geraldine2_baille.dae'
	];
	config_sysiphus_avatar.name_animations = [
		"idle",
		"arms_crossed",
		"baille"
	];
	config_sysiphus_avatar.name_default_animation = "idle";
	config_sysiphus_avatar.path_deforms = [];
	config_sysiphus_avatar.ready_callbak = on_avatar_ready;
	config_sysiphus_avatar.weight_speed = 0.5;
	config_sysiphus_avatar.debug_skeleton = true;
	config_sysiphus_avatar.diffuse_map = 'textures/avatar_normal.png';
	config_sysiphus_avatar.specular_map = 'textures/avatar_specular.png';
	config_sysiphus_avatar.normal_map = 'textures/avatar_normal.png';

	// POUF
	var config_pantonpouf = new PantonPoufConfig();
	config_pantonpouf.morph_path = 'models/pouf.json';
	config_pantonpouf.morph_default = 'basis';
	config_pantonpouf.butt_anim_path = 'models/butt_anims.json';
	config_pantonpouf.falling_anim_path = 'models/pouf_falling.json';
	config_pantonpouf.diffuse_map = 'textures/crash_diffuse.png';
	// config_pantonpouf.specular_map = 'textures/cube_specular.png';
	// config_pantonpouf.normal_map = 'textures/cube_normal.png';
	config_pantonpouf.left_foot_anchor = { 'pos': new THREE.Vector3( 0.24,.07,0.13 ), 'euler': new THREE.Euler( -0.95,0,Math.PI, 'XYZ' ) };
	config_pantonpouf.right_foot_anchor = { 'pos': new THREE.Vector3( -0.24,.07,0.13 ), 'euler': new THREE.Euler( -0.95,0,Math.PI, 'XYZ' ) };
	config_pantonpouf.weight_speed = 2.5;
	config_pantonpouf.default_visibility = false;
	config_pantonpouf.debug_anchors = false;
	config_pantonpouf.ready_callbak = on_pouf_ready;

	// *********************************
	// ************ GLOBALS ************
	// *********************************

	var container, stats, clock, controls;
	var camera, fps_camera, scene, renderer;
	var effectFXAA, bloomPass, renderScene;
	var composer;

	var SCENE_LOADED = false;
	var fps_camera_active = false;

	var sky;
	var ground = undefined;
	var avatar = undefined;
	var pouf = undefined;
	var tmngr = undefined;

	// debug objects

	var lightShadowMapViewer;

	var axis_3d = undefined;
	var frame_count = 0;

	// animation controls

	var head_tracker = undefined;

	var stats_enabled = undefined;

	var default_camera_position = new THREE.Vector3( 0, -0.4, 5 );
	
	var target_camera_position = undefined;
	var current_camera_position = undefined;
	var target_camera_target = new THREE.Vector3( 0, 0.75, 0 );
	var current_camera_target = undefined;
	
	var camera_position_transition_time = 1.0;
	var camera_position_transition_current = 0;

	var looking_min_delay = 5;
	var looking_max_delay = 50;
	var looking_duration = 10;
	var looking_timer = 0;
	var looking_enabled = false;

	var light_pivot = undefined;
	var ambient_light = undefined;
	var main_light = undefined;
	var second_light = undefined;

	var inventory_scrolltop = 0;
	var menu_close_timer = -1;

}

// *********************************
// *********** FUNCTIONS ***********
// *********************************

function reset_camera() {
	camera.position.set( default_camera_position.x, default_camera_position.y, default_camera_position.z );
}

function init_controls() {
	
	controls = new THREE.OrbitControls( camera, renderer.domElement );
	controls.target.set( 0, 1, 0 );
	controls.enableZoom = false;
	controls.enablePan = false;
	controls.enableDamping = true;
	controls.dampingFactor = 0.1;
	// controls.minPolarAngle = Math.PI * -0.8;
	// controls.maxPolarAngle = Math.PI * 0.52;
	controls.rotateSpeed = .078;
	controls.update();
	
}

function init_shadow_viewer() {
	
	lightShadowMapViewer = new THREE.ShadowMapViewer( main_light );
	lightShadowMapViewer.position.x = 10;
	lightShadowMapViewer.position.y = window.innerHeight - 260;
	lightShadowMapViewer.size.width = 250;
	lightShadowMapViewer.size.height = 250;
	lightShadowMapViewer.update();
	
}

function init_threejs() {

	container = document.getElementById( 'container' );
	camera = new THREE.PerspectiveCamera( 25, window.innerWidth / window.innerHeight, 1, 10000 );
	reset_camera();

	scene = new THREE.Scene();

	clock = new THREE.Clock();

	// lightning
	light_pivot = new THREE.Object3D();
	scene.add( light_pivot );

	ambient_light = new THREE.AmbientLight( 0xfff2ee, 0.32 );
	light_pivot.add( ambient_light );

	main_light = new THREE.DirectionalLight( 0xfdfdff, 0.9 );
	if ( SHADOW_ENABLED ) {
		main_light.castShadow = SHADOW_ENABLED;
		main_light.shadow = new THREE.LightShadow( new THREE.PerspectiveCamera( 25, 1, 2.5, 9 ) );
		main_light.shadow.bias = 0.0001;
		main_light.shadow.mapSize.width = SHADOW_MAP_WIDTH;
		main_light.shadow.mapSize.height = SHADOW_MAP_HEIGHT;
	}
	var pmult = 8;
	main_light.position.set( 0.4 * pmult, 0.5 * pmult, 0.2 * pmult );
	main_light.rotation.x = 1;
	light_pivot.add( main_light );
	
	init_shadow_viewer();
	
	second_light = new THREE.DirectionalLight( 0xfdfdff, 0.2 );
	// second_light = new THREE.DirectionalLight( 0xffffff, 0 );
	second_light.position.set( -1.2, 0, 0.5 );
	light_pivot.add( second_light );

	// renderer
	renderer = new THREE.WebGLRenderer( { antialias: true } );
	renderer.setPixelRatio( window.devicePixelRatio );
	renderer.setSize( window.innerWidth, window.innerHeight );
	renderer.setClearColor( new THREE.Color( 0x000000 ), 1 );
	if ( SHADOW_ENABLED ) {
		renderer.shadowMap.enabled = true;
		renderer.shadowMap.type = THREE.PCFShadowMap;
	}
	container.appendChild( renderer.domElement );

	// camera controls
	init_controls();

	track_head = new THREE.Object3D();
	scene.add( track_head );

	tmngr = new TimeManager();
	tmngr.init();

	// ground
	ground = new Ground();
	ground.configure( config_ground );
	ground.load( scene );

	// avatar
	avatar = new SysiphusAvatar();
	avatar.configure( config_sysiphus_avatar );
	avatar.load( scene );

	// pouf
	pouf = new PantonPouf();
	pouf.configure( config_pantonpouf );
	pouf.load( scene );

	head_tracker = new Tracker();
	head_tracker.init( 'head', scene );
	head_tracker.add_track( 'camera', camera.position );

	if ( stats_enabled !== undefined ) {
		stats = new Stats();
		container.appendChild( stats.dom );
	}

	// effects configuration
	scene.fog = new THREE.Fog( 0x000000, 3, 15 );
	// bloomPass.threshold = 0.55;
	// bloomPass.strength = 0.2;
	// bloomPass.radius = 0.04;
	// renderer.toneMappingExposure = Math.pow( 1.01, 4.0 );

	// events
	window.addEventListener( 'resize', onWindowResize, false );

}

function on_avatar_ready() {

	// if ( config_sysiphus_avatar.path_model == 'models/geraldine2.dae' ) {
	//
	// 	var gmat = new THREE.MeshPhongMaterial({
	// 		color: 0xf7ff00,
	// 		skinning: true,
	// 		flatShading: true,
	// 		reflectivity: 0
	// 	});
	// 	avatar.obj3d_mesh.material = gmat;
	//
	// } else {
	//
	// 	avatar.obj3d_mesh.material.shininess = 10;
	// 	avatar.obj3d_mesh.material.reflectivity = 0;
	//
	// }

	// attach the fps camera to the head
	fps_camera = new THREE.PerspectiveCamera( 80, window.innerWidth / window.innerHeight, 0.13, 10000 );
	avatar.bones["mixamorig_Head"].bone.add( fps_camera );
	fps_camera.position.y = 0.15;
	fps_camera.position.z = 0.05;
	fps_camera.quaternion.set(0,0,0,1);
	fps_camera.rotateY( Math.PI );
	fps_camera.rotateX( -0.3 );
	// var help = new THREE.CameraHelper( fps_camera );
	// scene.add( help );
	main_light.target = avatar.bones["mixamorig_Spine"].bone;

	if ( ground !== undefined && pouf !== undefined && ground.ready && pouf.ready ) {
		on_all_ready();
	}

}

function on_pouf_ready() {

	// var normalmat = new THREE.MeshNormalMaterial({
	// 	shininess: 100,
	// 	morphTargets: true,
	// 	morphNormals: true
	// })
	// pouf.morphable.obj3d.material = normalmat;

	if ( ground !== undefined && avatar !== undefined && ground.ready && avatar.ready ) {
		on_all_ready();
	}

}

function on_ground_ready() {

	ground.obj3d.rotateY( Math.PI * 0.25 );

	if ( pouf !== undefined && avatar !== undefined && pouf.ready && avatar.ready ) {
		on_all_ready();
	}

}

function on_all_ready() {

	// initialise track_head_targets
	head_tracker.add_track( 'pouf', pouf.morphable.obj3d );
	head_tracker.add_influence( avatar, 'mixamorig_Spine1', 0.1 );
	head_tracker.add_influence( avatar, 'mixamorig_Spine2', 0.4 );
	head_tracker.add_influence( avatar, 'mixamorig_Neck', 0.8 );
	head_tracker.add_influence( avatar, 'mixamorig_Head', 1 );

	pouf.move( 0, 0, -0.225 );
	avatar.move( 0, -0.005, 0.325 );
	pouf.scale( 0.24, 0.24, 0.24 );

	// render();
	// tmngr.start();
	// toggle_camera();

}

function number_display( num ) {

	var str = "" + Math.floor( num * 100 ) / 100;
	var n = str.indexOf(".");
	if ( n == -1 ) {
		return str + ".00";
	} else if	( n == str.length - 2 ) {
		return str + "0";
	}
	return str;

}

function onWindowResize() {

	camera.aspect = window.innerWidth / window.innerHeight;
	camera.updateProjectionMatrix();
	fps_camera.aspect = window.innerWidth / window.innerHeight;
	fps_camera.updateProjectionMatrix();
	renderer.setSize( window.innerWidth, window.innerHeight );
	//composer.setSize( window.innerWidth, window.innerHeight );
	//fps_controls.handleResize();
	
	init_shadow_viewer();

}

function animate() {

	requestAnimationFrame( animate );
	render();
	if ( stats_enabled !== undefined ) {
		stats.update();
	}

}

function toggle_camera() {

	if ( !avatar.ready || !pouf.ready ) {
		return;
	}

	if ( fps_camera_active ) {
		// tmngr.camera_tracking_enable(true);
		fps_camera_active = false;
	} else {
		// tmngr.camera_tracking_enable(false);
		fps_camera_active = true;
	}

}

function render() {

	var delta = clock.getDelta();

	if ( avatar.ready === true && pouf.ready === true ) {

		if ( !tmngr.ready ) {
			tmngr.start();
		}

		light_pivot.rotation.y = controls.getAzimuthalAngle();

		Interpolation.update( delta );
		tmngr.update( delta );
		if ( tmngr.morph_enabled && timestamp_data !== undefined ) {
			working_time();
		}

		head_tracker.update( delta );
		avatar.update( delta );
		pouf.update( delta );

	}
	
	if ( target_camera_position != undefined ) {
		
		camera_position_transition_current += delta;
		var pc = camera_position_transition_current / camera_position_transition_time;
		if ( pc > 1 ) {
			pc = 1;
		}
		
		var mu = ( 1 + Math.sin( -Math.PI * 0.5 + Math.PI * pc ) ) * 0.5;
		
		var newp = current_camera_position.clone();
		newp.lerp( target_camera_position, mu );
		camera.position.set( newp.x, newp.y, newp.z );
		
		var newt = current_camera_target.clone();
		newt.lerp( target_camera_target, mu );
		controls.target.set( newt.x, newt.y, newt.z );
		
		if ( pc == 1 ) {
			target_camera_position = undefined;
		}
		
	}
	
	controls.update();
	
	if ( fps_camera_active ) {
		renderer.render( scene, fps_camera );
		lightShadowMapViewer.render( renderer );
	} else {
		renderer.render( scene, camera );
	}
	
	if ( menu_close_timer > 0 ) {
		menu_close_timer -= delta;
		if ( menu_close_timer <= 0 && !mainmenu_visible() ) {
			goto_mainmenu();
		}
	}

	// composer.render();

	// frame_count++;

}

function init_timeline() {
	document.getElementById( 'timeline' ).style.visibility = 'visible';
}

function working_time() {
	var content = "production time: ";
	var millis = tmngr.morph_inter.current * timestamp_data['max'];
	var secs = millis / 1000;
	var h = Math.floor( secs / 3600 );
	var m = Math.floor( ( secs - h * 3600 ) / 60 );
	var s = Math.floor( secs - ( h * 3600 + m * 60 ) );
	if ( h < 10 ) content += "0";
	content += h + ":";
	if ( m < 10 ) content += "0";
	content += m + ":";
	if ( s < 10 ) content += "0";
	content += s;
	document.getElementById( 'production_duration' ).innerHTML = content;
}

function hide_timeline() {
	document.getElementById( 'timeline' ).style.visibility = 'hidden';
}

function print( e ) {}

function debug( e ) {
	document.getElementById("debugger").innerHTML = e;
}

function debug_json( e ) {
	document.getElementById("debugger").innerHTML = JSON.stringify(e, null, 4);
}

// *********************************
// ******* QUESTION RELATED ********
// *********************************

// https://stackoverflow.com/questions/2368784/draw-on-html5-canvas-using-a-mouse
// https://stackoverflow.com/questions/3437786/get-the-size-of-the-screen-current-web-page-and-browser-window

var q_canvas, q_ctx, q_flag = false,
		q_prevX = 0,
		q_currX = 0,
		q_prevY = 0,
		q_currY = 0,
		q_dot_flag = false;

var q_margins = [ 120, 50, 100, 50 ];
var q_ruler_gap = 50;

var q_x = "yellow",
		q_y = 4;

function question_size() {
	var w = window,
		d = document,
		e = d.documentElement,
		g = d.getElementsByTagName('body')[0],
		x = w.innerWidth || e.clientWidth || g.clientWidth,
		y = w.innerHeight|| e.clientHeight|| g.clientHeight;
	return [x,y];
}

function question_frame() {

	var dim = question_size();

	q_ctx.fillStyle = "white";
	q_ctx.font = "20px lincolnmitrelm";
	q_ctx.fillText("What size", 50, 70 );
	q_ctx.fillText("is a banana?", 50, 95 );
	q_ctx.fillText("draw:", dim[0]-100, 95 );

	var counter = 1;
	for ( var i = dim[1]-(q_margins[2] + q_ruler_gap); i > q_margins[0] + 20; i -= q_ruler_gap ) {
		q_ctx.fillText( counter, 75, i + 10 );
		counter++;
	}

	q_ctx.beginPath();
	q_ctx.moveTo( q_margins[3], q_margins[0] );
	q_ctx.lineTo( dim[0]-q_margins[1], q_margins[0] );
	q_ctx.lineTo( dim[0]-q_margins[1], dim[1]-q_margins[2] );
	q_ctx.lineTo( q_margins[3], dim[1]-q_margins[2] );
	q_ctx.lineTo( q_margins[3], q_margins[0] );

	for ( var i = dim[1]-(q_margins[2] + q_ruler_gap); i > q_margins[0] + 20; i -= q_ruler_gap ) {
		q_ctx.moveTo( 50, i );
		q_ctx.lineTo( 65, i );
	}

	q_ctx.strokeStyle = "white";
	q_ctx.lineWidth = 2;
	q_ctx.stroke();
	q_ctx.closePath();

}

function question_init() {

	document.getElementById( 'question' ).style.visibility = 'visible';

	q_canvas = document.getElementById('question_canvas');
	var dim = question_size();
	q_ctx = q_canvas.getContext("2d");
	q_ctx.canvas.width = dim[0];
	q_ctx.canvas.height = dim[1];
	question_frame();

	q_canvas.addEventListener("mousemove", function (e) {
	question_findxy('move', 'mouse', e)
	}, false);
	q_canvas.addEventListener("mousedown", function (e) {
	question_findxy('down', 'mouse', e)
	}, false);
	q_canvas.addEventListener("mouseup", function (e) {
	question_findxy('up', 'mouse', e)
	}, false);
	q_canvas.addEventListener("mouseout", function (e) {
	question_findxy('out', 'mouse', e)
	}, false);

	q_canvas.addEventListener("touchmove", function (e) {
	question_findxy('move', 'touch', e);
	preventDefault(e);
	}, false);
	q_canvas.addEventListener("touchstart", function (e) {
	question_findxy('down', 'touch', e);
	preventDefault(e);
	}, false);
	q_canvas.addEventListener("touchend", function (e) {
	question_findxy('up', 'touch', e);
	preventDefault(e);
	}, false);
	q_canvas.addEventListener("touchcancel", function (e) {
	question_findxy('out', 'touch', e);
	preventDefault(e);
	}, false);

	question_frame();

	window.addEventListener( 'resize', question_resize, false );

	// setTimeout(function(){ question_frame() }, 1000);
	// setTimeout(function(){ question_frame() }, 2000);
	// setTimeout(function(){ question_frame() }, 3000);
	// setTimeout(function(){ question_frame() }, 5000);

}

function preventDefault(e) {
	e = e || window.event;
	if (e.preventDefault)
		e.preventDefault();
	e.returnValue = false;
}

function question_resize() {

	var dim = question_size();
	q_ctx.canvas.width = dim[0];
	q_ctx.canvas.height = dim[1];
	question_frame();

}

function question_retrieve_pos( type, e ) {
	if ( type == 'mouse' ) {
		q_currX = e.clientX - q_canvas.offsetLeft;
		q_currY = e.clientY - q_canvas.offsetTop;
	} else if ( type == 'touch' ) {
		q_currX = e.changedTouches[0].pageX - q_canvas.offsetLeft;
		q_currY = e.changedTouches[0].pageY - q_canvas.offsetTop;
	}
}

function question_findxy(res, type, e) {

		if (res == 'down') {
				q_prevX = q_currX;
				q_prevY = q_currY;
				question_retrieve_pos( type, e );
				q_flag = true;
				q_dot_flag = true;
				if (q_dot_flag) {
						q_ctx.beginPath();
						q_ctx.fillStyle = q_x;
						q_ctx.fillRect( q_currX, q_currY, 2, 2 );
						q_ctx.closePath();
						q_dot_flag = false;
				}
		}

		if (res == 'up' || res == "out") {
				q_flag = false;
		}

		if (res == 'move') {
				if (q_flag) {
						q_prevX = q_currX;
						q_prevY = q_currY;
						question_retrieve_pos( type, e );
						question_draw();
				}
		}

}

function question_draw() {
		q_ctx.beginPath();
		q_ctx.moveTo( q_prevX, q_prevY );
		q_ctx.lineTo( q_currX, q_currY );
		q_ctx.strokeStyle = q_x;
		q_ctx.lineWidth = q_y;
		q_ctx.stroke();
		q_ctx.closePath();
		// console.log( "question_draw()", q_prevX, q_prevY, q_currX, q_currY );
}

function question_ok() {

	if ( !skip_splash ) {
		//document.getElementById( 'question' ).outerHTML = '';
		//document.getElementById( 'question' ).style.visibility = 'hidden';
	}
	document.getElementById( 'ui_3d' ).style.visibility = 'visible';
	document.getElementById( 'timeline' ).style.visibility = 'visible';
	window.addEventListener( 'resize', onWindowResize, false );
	animate();

}

// *********************************
// ********** UI RELATED ***********
// *********************************

function mainmenu_visible() {
	
	return document.getElementById( 'ui_3d' ).style.visible == 'visible';

}

function menu_stop( el ) {
	
	el.className = '';
	el.style.visibility = 'hidden';
	
}

function menu_start( el ) {
	
	el.style.visibility = 'visible';
	el.style.opacity = '1';
	el.style.animation='menu_opening_anim 0.5s ease-out';
	var newone = el.cloneNode(true);
	el.parentNode.replaceChild(newone, el);
	
}

function goto_mainmenu() {
	
	if ( document.getElementById( 'inventory_preview' ).style.visibility != 'hidden' ) {
		inventory_scrolltop = document.getElementById("inventory_preview").scrollTop;
	}
	
	menu_stop( document.getElementById( 'about_panel' ) );
	menu_stop( document.getElementById( 'inventory_preview' ) );
	menu_start( document.getElementById( 'ui_3d' ) );
	document.getElementById( 'container' ).className = '';
	
}

function goto_about() {
	
	menu_stop( document.getElementById( 'ui_3d' ) );
	menu_stop( document.getElementById( 'inventory_preview' ) );
	menu_start( document.getElementById( 'about_panel' ) );
	document.getElementById( 'container' ).className = 'blurry';
	
	if (typeof MENU_TIMEOUT !== 'undefined') {
		menu_close_timer = MENU_TIMEOUT;
	}
	
}

function goto_inventory() {

	gallery_request( true );
	menu_stop( document.getElementById( 'ui_3d' ) );
	menu_stop( document.getElementById( 'about_panel' ) );
	menu_start( document.getElementById( 'inventory_preview' ) );
	document.getElementById( 'container' ).className = 'blurry';
	document.getElementById("inventory_preview").scrollTop = inventory_scrolltop;
	
	if (typeof MENU_TIMEOUT !== 'undefined') {
		menu_close_timer = MENU_TIMEOUT;
	}

}

var tabs_ids = [ 'gallery', 'info', 'credits' ];
var tabs_content = [];

function tabs_toggle( id ) {

	// open or close?
	var div = document.getElementById( tabs_ids[id] + "_panel");
	if ( div.className == "displayable_open" ) {
		document.getElementById( "panel" ).className = "panel_closed";
		document.getElementById( tabs_ids[id] + "_link").className = "tab_normal";
		div.className = "displayable";
		return;
	}

	document.getElementById( "panel" ).className = "panel_open";

	var i = 0;
	var l = tabs_ids.length;
	while ( i < l ) {
		var tname = tabs_ids[i];
		if ( id == i ) {
			document.getElementById( tname + "_link").className = "tab_selected";
			document.getElementById( tname + "_panel").className = "displayable_open";
		} else {
			document.getElementById( tname + "_link").className = "tab_normal";
			document.getElementById( tname + "_panel").className = "displayable";
		}
		i++;
	}

}

function add_gallery_item( index ) {

	if ( gallery_data === undefined ) {
		return;
	}

	if ( index >= gallery_data.length ) {
		return;
	}

	var gelm = document.getElementById("inventory_list");

	var d = gallery_data[index];
	var info = [];
	var t = [];

	if ( d && d.length > 0 ) {
		info = d.split("|");
		if ( info.length >= 3 ) {
			t = info[2].split("-");
		}
		if ( t.length != 4 ) {
			info = [];
			t = [];
		}
	}

	if ( info.length >= 3 ) {

		if ( t[0].length == 1 ) t[0] = "0"+t[0];
		if ( t[1].length == 1 ) t[1] = "0"+t[1];
		if ( t[2].length == 1 ) t[2] = "0"+t[2];
		if ( t[3].length == 1 ) t[3] = "0"+t[3];

		var alttext = 'chair ' + index + ', produced on the ' + info[1] +
			" in " +
			t[0] + " day(s), " +
			t[1] + " hour(s), " +
			t[2] + " minute(s) and " +
			t[3] + " second(s)";

		var div = document.createElement("div");
			div.id = 'chair' + index;
			var img = document.createElement("img");
			if ( info.length == 3 ) {
				div.className = 'chair_preview placeholder';
				img.src = 'gallery/'+info[0]+'.png';
			} else {
				div.className = 'chair_preview';
				img.src = 'models/generated/'+info[3]+'_icon.png';
			}
				div.appendChild( img );
			var content = document.createElement("div");
				content.className = 'content';
				content.alt = alttext;
				content.title = alttext;
				var wrapper = document.createElement("span");
					wrapper.className = 'wrapper';
					var date = document.createElement("span");
						date.className = 'date';
						date.innerHTML = info[1] + '<br/>';
						wrapper.appendChild( date );
					var time = document.createElement("span");
						time.className = 'time';
						time.innerHTML = t[0] + ':' + t[1] + ':' + t[2] + ':' + t[3];
						wrapper.appendChild( time );
				content.appendChild( wrapper );
			div.appendChild( content );
		gelm.appendChild( div );

	}

	setTimeout( function() { add_gallery_item( index+1 ) }, 5 );

}

function update_gallery() {

	if ( gallery_data === undefined ) {
		return;
	}

	var gelm = document.getElementById("inventory_list");

	for ( var i = 0; i < gallery_data.length; ++i ) {
		var id = 'chair' + i;
		var elm = document.getElementById(id);
		if ( elm === null ) {
			add_gallery_item( i );
			return;
		}
	}

	// var gelm = document.getElementById("inventory_list");
	// gelm.innerHTML = '';

}

function gallery_detail( path, data ) {
	var list = data.split( "|" );
	document.getElementById( "gallery_details_img" ).src = path;
	document.getElementById( "gallery_details_text" ).innerHTML =
		"<b>name</b>: " + list[0] +"<br/>"+
		"<b>date</b>: " + list[1] +"<br/>"+
		"<b>production time</b>: " + list[2] +""
		;
}

// *********************************
// ******* TIMESTAMP SERVER ********
// *********************************

var timestamp_data = undefined;

// this function is called at startup
function timestamp_request() {

	var xobj = new XMLHttpRequest();
	xobj.overrideMimeType("application/json");
	xobj.open( 'GET', remote_timestamp_server, true );
	xobj.onreadystatechange = function () {
		if (xobj.readyState == 4 && xobj.status == "200") {
			try{
				var data = JSON.parse( xobj.responseText );
				if ( timestamp_data === undefined ) {
					console.log( ">>>>> first reception of timestamp!!!" );
					init_timeline();
					timestamp_data = data;
				} else {
					timestamp_data = data;
				}
				// callback depends on data sent by the server
				setTimeout( function(){ timestamp_request(); }, data['revisit'] );
			} catch( exception ) {
				console.log( "Received crappy json from timestamp server" );
			}
		} else if ( xobj.status != "200" ) {
			console.log( "Unable to contact timestamp server!" );
			timestamp_data = undefined;
			hide_timeline();
			// retry in 30 seconds
			setTimeout( function(){ timestamp_request(); }, 30000 );
		}
	}
	xobj.send(null);

}

var gallery_data = [];
var gallery_server_enabled = false;

function gallery_request( update ) {

	var xobj = new XMLHttpRequest();
	xobj.overrideMimeType("application/json");
	xobj.open( 'GET', remote_gallery_server, true );
	xobj.onreadystatechange = function () {
		if (xobj.readyState == 4 && xobj.status == "200") {
			// try{
				var data = JSON.parse( xobj.responseText );
				// console.log( data );
				gallery_data = [];
				for( var i in data ) {
					gallery_data.push( data[i] );
				}
				gallery_server_enabled = true;
				if ( update ) { update_gallery(); }
			// } catch( exception ) {
			// 	console.log( "Received crappy json from gallery server" );
			// }
		} else if ( xobj.status != "200" ) {
			console.log( "Unable to contact gallery server!" );
			if ( update ) update_gallery();
			timestamp_data = undefined;
			// retry in 30 seconds
			// setTimeout( function(){ gallery_request(); }, 30000 );
		}
	}
	xobj.send(null);

}

// *********************************
// *********** BOOTING *************
// *********************************

if ( skip_splash ) {
	document.getElementById( 'splash' ).outerHTML = '';
	timestamp_request();
	gallery_request();
	init_threejs();
	question_ok();
} else {
	setTimeout( function(){ timestamp_request(); gallery_request(); }, 1000 );
	setTimeout( function(){ init_threejs(); }, 2000 );
	//setTimeout( function(){ question_init(); }, 5000 );
	setTimeout( function(){ close_splash(); question_ok(); }, 6500 );
}

function close_splash() {
	document.getElementById( 'splash' ).outerHTML = '';
}
