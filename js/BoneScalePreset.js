/*

 _________ ____  .-. _________/ ____ .-. ____
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'

 libre video game bazaar

 ____________________________________  ?   ____________________________________
                                     (._.)


 This file is part of sysiphus project
 For the latest info, see http://polymorph.cool/

 Copyright (c) 2018 polymorph.cool
 All rights reserved.

 MIT License

 Permission is hereby granted, free of charge, to any person obtaining a copy of this
 software and associated documentation files (the "Software"), to deal in the Software
 without restriction, including without limitation the rights to use, copy, modify, merge,
 publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED *AS IS*, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.

 ___________________________________( ^3^)_____________________________________

 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)

*/

function BoneScalePreset() {

	this.json = undefined;
	this.name = undefined;
	this.bone_scale = undefined;

	this.init = function( bones, json ) {
		this.bone_scale = {};
		if ( json === undefined ) {
			this.unit( bones );
			return;
		}
		this.json = json;
		this.name = json.name;
		var default_scale = this.parse_bone_vec3("__default__");
		var found = false;
		for ( var bn in bones ) {
			found = false;
			for ( var bscn in json.bones ) {
				if ( bn == bscn ) {
					this.bone_scale[bn] = this.parse_bone_vec3(bn);
					found = true;
					break;
				}
			}
			if ( found ) {
				continue;
			}
			this.bone_scale[bn] = default_scale.clone();
		}
		// console.log(this.bone_scale);
	};

	this.unit = function( bones ) {
		this.name = "__UNIT__";
		for ( var bn in bones ) {
			this.bone_scale[bn] = new THREE.Vector3( 1,1,1 );
		}
	}

	this.parse_bone_vec3 = function( name ) {
		return new THREE.Vector3(
			this.json.bones[name]["vec3"][0],
			this.json.bones[name]["vec3"][1],
			this.json.bones[name]["vec3"][2]);
	};

	/* @arg 'bsp_src': a BoneScalePreset, it will be used as the origin
	 * @arg 'bone_name': a string, the name of the bone in both BoneScalePreset
	 * @arg 'alpha': a float, if 0, origin scale is returned, if 1, the local scale is returned
	 */
	this.lerp = function( bsp_src, bone_name, alpha ) {
		var v3 = bsp_src.bone_scale[bone_name].clone();
		return v3.lerp( this.bone_scale[bone_name], alpha );
	};

};
