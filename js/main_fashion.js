/*
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      

 libre video game bazaar

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of sysiphus project
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2018 polymorph.cool
 All rights reserved. 
 
 MIT License

 Permission is hereby granted, free of charge, to any person obtaining a copy of this
 software and associated documentation files (the "Software"), to deal in the Software 
 without restriction, including without limitation the rights to use, copy, modify, merge,
 publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 to whom the Software is furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED *AS IS*, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.
 
 ___________________________________( ^3^)_____________________________________
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
*/

if ( ! Detector.webgl ) Detector.addGetWebGLMessage();

// *********************************
// ************ CONFIG *************
// *********************************

var config_sysiphus_avatar = new SysiphusAvatarConfig();
config_sysiphus_avatar.mesh = "Body_Idle";
config_sysiphus_avatar.skeleton_root = "Armature_breathing";
config_sysiphus_avatar.path_model = 'models/geraldine.dae';
config_sysiphus_avatar.path_animations = ['models/geraldine_idle.dae','models/geraldine_sleeping.dae','models/geraldine_sleeping_back.dae'];
config_sysiphus_avatar.name_animations = ["idle","sleeping side","sleeping back"];
config_sysiphus_avatar.name_default_animation = "idle";
config_sysiphus_avatar.path_deforms = ['presets/deform_test.json','presets/deform_test2.json'];

// *********************************
// ************ GLOBALS ************
// *********************************

var stats_enabled = true;

var container, stats, clock, controls;
var camera, scene, renderer;

var gridHelper = undefined;

var SCENE_LOADED = false;

var avatar = undefined;

// debug objects
var frame_count = 0;

var ui_settings = undefined;

var texture_canvas = undefined;
var texture_context = undefined;
var texture_texture = undefined;
var texture_cellnum = 32;


var texture_time = 0;

var texture_gradients = {};
texture_gradients["RG"] = { 
	type: "linear", 
	pos: [ 0,0,0,1 ], 
	stops: [
		[0.6,[1,1,1,1]],
		[1.0,[0,0,0,1]]
	], 
	obj: undefined };

texture_gradients["rad"] = { 
	type: "radial", 
	pos: [ (612/2048),(1522/2048),0,(612/2048),(1800/2048),(409/2048) ], 
	stops: [
		[0.0,[1,0,0,1]],
		[1.0,[0,0,1,0.01]]
	], 
	obj: undefined };

var texture_areas = {};
// texture_areas["head"] = { rects: [[0, 0, 0.497070313, 0.397460938]], fill: "#ffffff" };
texture_areas["torso"] = { rects: [[0,0.509765625, 0.595214844, 0.490234375]], gradient: "rad" };
// texture_areas["legs"] = { rects: [[0.6015625, 0.599609375, 0.3984375, 0.400390625]], fill: "#ffffff" };
// texture_areas["arms"] = { rects: [[0.599609375, 0.015625, 0.400390625, 0.572265625]], fill: "#ffffff" };
// texture_areas["feet"] = { rects: [[0.1015625, 0.3984375, 0.396484375, 0.19921875]], fill: "#ffffff" };
// texture_areas["neck"] = { rects: [[(15/2048), (616/2048), (999/2048), (199/2048)]], fill: "#000000" };

texture_areas["spine"] = { rects: [
	[(19/2048), (1388/2048), (54/2048), (645/2048)],
	[(1153/2048), (1388/2048), (54/2048), (645/2048)],
	[(35/2048), (1062/2048), (165/2048), (60/2048)],
	[(1034/2048), (1062/2048), (165/2048), (63/2048)],
	[(10/2048), (407/2048), (51/2048), (409/2048)],
	[(966/2048), (407/2048), (51/2048), (409/2048)],
	], 
	rgba: [0,1,1,1] };

texture_areas["elbow"] = { rects: [
	[(1452/2048), (226/2048), (110/2048), (374/2048)],
	[(1452/2048), (627/2048), (110/2048), (374/2048)],
	], 
	rgba: [1,0.3,0,1] };

texture_areas["wrist"] = { rects: [
	[(1660/2048), (211/2048), (144/2048), (394/2048)],
	[(1660/2048), (627/2048), (144/2048), (394/2048)],
	], 
	rgba: [1,0.3,0,1] };

texture_areas["knee"] = { rects: [
	[(1510/2048), (1240/2048), (144/2048), (409/2048)],
	[(1633/2048), (1630/2048), (144/2048), (409/2048)],
	], 
	rgba: [1,0.3,0,1] };

// *********************************
// *********** FUNCTIONS ***********
// *********************************

init();

animate();

function init() {

	container = document.getElementById( 'container' );
	camera = new THREE.PerspectiveCamera( 25, window.innerWidth / window.innerHeight, 1, 10000 );
	camera.position.set( 5, 2.5, 5 );
	scene = new THREE.Scene();
	clock = new THREE.Clock();

	gridHelper = new THREE.GridHelper( 10, 20 );
	scene.add( gridHelper );

	// lightning
	var ambientLight = new THREE.AmbientLight( 0xffffff, 0.2 );
	scene.add( ambientLight );
	var directionalLight = new THREE.DirectionalLight( 0xffffff, 0.8 );
	directionalLight.position.set( 1, 1, 1 );
	scene.add( directionalLight );

	// renderer
	renderer = new THREE.WebGLRenderer( { antialias: true } );
	renderer.setPixelRatio( window.devicePixelRatio );
	renderer.setSize( window.innerWidth, window.innerHeight );
	renderer.setClearColor( new THREE.Color( 0xaaaaaa ), 1 );
	container.appendChild( renderer.domElement );

	// camera controls
	controls = new THREE.OrbitControls( camera, renderer.domElement );
	controls.target.set( 0, 2, 0 );
	controls.update();

	// stats
	stats = undefined;
	if ( stats_enabled ) {
		stats = new Stats();
		container.appendChild( stats.dom );
	}

	// debug
	var geometry;
	var material;
	var wireframe;

	// avatar
	avatar = new SysiphusAvatar();
	avatar.ready_callbak = on_avatar_ready;
	avatar.configure( config_sysiphus_avatar );
	avatar.load( scene );

	gridHelper.position.set( 0, 1, 0 );

	// events
	window.addEventListener( 'resize', onWindowResize, false );
}

function to_rgba( rgba ) {
	return "rgba(" + 
				(rgba[0]*255) +","+
				(rgba[1]*255) +","+
				(rgba[2]*255) +","+
				(rgba[3]*255) +")";
}

function lerp_rgba( rgba1, rgba2, mult = 1 ) {
	var multi = 1 - mult;
	return "rgba(" + 
				((rgba1[0]*multi + rgba2[0]*mult)*255) +","+
				((rgba1[1]*multi + rgba2[1]*mult)*255) +","+
				((rgba1[2]*multi + rgba2[2]*mult)*255) +","+
				((rgba1[3]*multi + rgba2[3]*mult)*255) +")";
}

function create_texture_canvas() {

	texture_canvas = document.createElement("canvas");
	texture_canvas.width = 256;
	texture_canvas.height = 256;
	texture_context = texture_canvas.getContext("2d");

	for ( var key in texture_gradients ) {
		var gradient = texture_gradients[key];
		if ( gradient.type == "linear" ) {
			gradient.obj=texture_context.createLinearGradient(
				gradient.pos[0] * texture_canvas.width, 
				gradient.pos[1] * texture_canvas.height,
				gradient.pos[2] * texture_canvas.width, 
				gradient.pos[3] * texture_canvas.height
				);
		} else if  ( gradient.type == "radial" ) {
			gradient.obj=texture_context.createRadialGradient(
				gradient.pos[0] * texture_canvas.width, 
				gradient.pos[1] * texture_canvas.height,
				gradient.pos[2] * texture_canvas.width,
				gradient.pos[3] * texture_canvas.width, 
				gradient.pos[4] * texture_canvas.height, 
				gradient.pos[5] * texture_canvas.width
				);
		}
		if ( gradient.obj !== undefined ) {
			for( var i in gradient.stops ) {
				var pc = 
				gradient.obj.addColorStop( gradient.stops[i][0], to_rgba( gradient.stops[i][1] ) );
			}
			console.log( gradient.obj );
		}
	}
	
	// background
	texture_context.fillStyle = "#000000";
	texture_context.fillRect(0, 0, texture_canvas.width, texture_canvas.height);

	// areas
	for( var key in texture_areas ) {

		var area = texture_areas[key];

		if ( area.gradient !== undefined ) {
			area.fill = texture_gradients[area.gradient].obj;
		} else if ( area.rgba !== undefined ) {
			area.fill = to_rgba( area.rgba );
		}

		for ( var i in area.rects ) {

			var rect = area.rects[i];
			texture_context.fillStyle = area.fill;
			texture_context.fillRect(
				rect[0] * texture_canvas.width,
				rect[1] * texture_canvas.height,
				rect[2] * texture_canvas.width,
				rect[3] * texture_canvas.height
				);

		}

	}

	texture_texture = new THREE.Texture(texture_canvas);
	texture_texture.anisotropy = 4;
	texture_texture.needsUpdate = true;

}

function on_avatar_ready() {

	avatar.move( 0, 1, 0 );
	console.log( avatar.obj3d_mesh.material );

	// http://collaboradev.com/2016/03/17/drawing-on-textures-in-threejs/
	create_texture_canvas();

	avatar.obj3d_mesh.material.map = texture_texture;
	avatar.obj3d_mesh.material.shininess = 2;
	avatar.obj3d_mesh.material.reflectivity = 0;

}

function onWindowResize() {

	camera.aspect = window.innerWidth / window.innerHeight;
	camera.updateProjectionMatrix();
	renderer.setSize( window.innerWidth, window.innerHeight );

}

function animate() {

	requestAnimationFrame( animate );
	render();
	if ( stats_enabled ) {
		stats.update();
	}

}

function render_texture( delta ) {

	texture_time += delta * 3;
	var mult = ( 1 + Math.sin( texture_time ) ) * 0.5;
	var bg = [0,0,1,1];

	for ( var key in texture_gradients ) {
		var gradient = texture_gradients[key];
		if ( gradient.type == "linear" ) {
			gradient.obj=texture_context.createLinearGradient(
				gradient.pos[0] * texture_canvas.width, 
				gradient.pos[1] * texture_canvas.height,
				gradient.pos[2] * texture_canvas.width, 
				gradient.pos[3] * texture_canvas.height
				);
		} else if  ( gradient.type == "radial" ) {
			gradient.obj=texture_context.createRadialGradient(
				gradient.pos[0] * texture_canvas.width, 
				gradient.pos[1] * texture_canvas.height,
				gradient.pos[2] * texture_canvas.width,
				gradient.pos[3] * texture_canvas.width, 
				gradient.pos[4] * texture_canvas.height, 
				gradient.pos[5] * texture_canvas.width
				);
		}
		if ( gradient.obj !== undefined ) {
			for( var i in gradient.stops ) {
				var pc = 
				gradient.obj.addColorStop( gradient.stops[i][0], lerp_rgba( bg, gradient.stops[i][1], mult ) );
			}
		}
	}


	// background
	texture_context.fillStyle = to_rgba( bg );
	texture_context.fillRect(0, 0, texture_canvas.width, texture_canvas.height);

	for( var key in texture_areas ) {

		var area = texture_areas[key];

		if ( area.gradient !== undefined ) {
			area.fill = texture_gradients[area.gradient].obj;
		} else if ( area.rgba !== undefined ) {
			area.fill = lerp_rgba( bg, area.rgba, mult );
		}

		for ( var i in area.rects ) {

			var rect = area.rects[i];
			texture_context.fillStyle = area.fill;
			texture_context.fillRect(
				rect[0] * texture_canvas.width,
				rect[1] * texture_canvas.height,
				rect[2] * texture_canvas.width,
				rect[3] * texture_canvas.height
				);

		}

	}
	texture_texture.needsUpdate = true;

}

function render() {

	var delta = clock.getDelta();

	if ( avatar.ready === true ) {

		render_texture( delta );
		avatar.update( delta );

	}

	renderer.render( scene, camera );

	frame_count++;

}

function print( e ) {}