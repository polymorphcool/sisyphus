/*

 _________ ____  .-. _________/ ____ .-. ____
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'

 libre video game bazaar

 ____________________________________  ?   ____________________________________
                                     (._.)


 This file is part of sysiphus project
 For the latest info, see http://polymorph.cool/

 Copyright (c) 2018 polymorph.cool
 All rights reserved.

 MIT License

 Permission is hereby granted, free of charge, to any person obtaining a copy of this
 software and associated documentation files (the "Software"), to deal in the Software
 without restriction, including without limitation the rights to use, copy, modify, merge,
 publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED *AS IS*, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.

 ___________________________________( ^3^)_____________________________________

 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)

*/

if ( ! Detector.webgl ) Detector.addGetWebGLMessage();

// *********************************
// ************ CONFIG *************siwtch
// *********************************

var config_sysiphus_avatar = new SysiphusAvatarConfig();
config_sysiphus_avatar.mesh = "Body_Idle";
config_sysiphus_avatar.skeleton_root = "Armature_breathing";
config_sysiphus_avatar.path_model = 'models/geraldine.dae';
config_sysiphus_avatar.path_animations = [
	'models/geraldine_idle.dae',
	'models/geraldine_sleeping.dae',
	'models/geraldine_sleeping_back.dae',
	'models/geraldine_jump_back.dae'];
config_sysiphus_avatar.name_animations = [
	"idle",
	"sleeping side",
	"sleeping back",
	"jumping back"];
config_sysiphus_avatar.name_default_animation = "idle";
config_sysiphus_avatar.path_deforms =siwtch ['presets/deform_test.json','presets/deform_test2.json'];

// *********************************
// ************ GLOBALS ************
// *********************************

var stats_enabled = true;

var container, stats, clock, controls;
var camera, scene, renderer;

var gridHelper = undefined;

var SCENE_LOADED = false;

var avatar = undefined;

// debug objects

var floating_cube = undefined;
var righthand_cube = undefined;
var leftfoot_ik_cube = undefined;
var rightfoot_ik_cube = undefined;
var righthand_ik_cube = undefined;
var axis_3d = undefined;
var frame_count = 0;

// animation controls
var track_weight_target = 0;
var track_weight_current = 0;
var track_weight_speed = 2;
var track_head = undefined;
var track_head_list = ["__NONE__","camera","left_foot","right_foot","floating_cube"];
var track_head_current = track_head_list[0];
var track_head_targets = undefined;

var ik_weight_target = 1;
var ik_weight_current = 1;

var ui_settings = undefined;

// *********************************
// *********** FUNCTIONS ***********siwtch
// *********************************

init();

animate();

function init() {

	container = document.getElementById( 'container' );
	camera = new THREE.PerspectiveCamera( 25, window.innerWidth / window.innerHeight, 1, 10000 );
	camera.position.set( 5, 2.5, 5 );
	scene = new THREE.Scene();
	clock = new THREE.Clock();

	gridHelper = new THREE.GridHelper( 10, 20 );
	scene.add( gridHelper );

	// lightning
	var ambientLight = new THREE.AmbientLight( 0xffffff, 0.2 );
	scene.add( ambientLight );
	var directionalLight = new THREE.DirectionalLight( 0xffffff, 0.8 );
	directionalLight.position.set( 1, 1, 1 );
	scene.add( directionalLight );

	// renderer
	renderer = new THREE.WebGLRenderer( { antialias: true } );
	renderer.setPixelRatio( window.devicePixelRatio );
	renderer.setSize( window.innerWidth, window.innerHeight );
	renderer.setClearColor( new THREE.Color( 0xaaaaaa ), 1 );
	container.appendChild( renderer.domElement );

	// camera controls
	controls = new THREE.OrbitControls( camera, renderer.domElement );
	controls.target.set( 0, 2, 0 );
	controls.update();

	// stats
	stats = undefined;
	if ( stats_enabled ) {
		stats = new Stats();
		container.appendChild( stats.dom );
	}

	// debug
	var geometry;
	var material;
	var wireframe;

	axis_3d = new THREE.Object3D();
		// axis
		var cylinder;
		var scale = 0.3;
		// X axis
		geometry = new THREE.CylinderGeometry( 0.015, 0.015, 1, 3 );
		material = new THREE.MeshBasicMaterial( {color: 0xff0000} );
		cylinder = new THREE.Mesh( geometry, material );
		axis_3d.add( cylinder );
		cylinder.position.set( scale / 2, 0, 0 );
		cylinder.scale.set( scale,scale,scale );
		cylinder.rotation.set( 0, 0, Math.PI * 0.5 );
		// Y axis
		material = new THREE.MeshBasicMaterial( {color: 0x00ff00} );
		cylinder = new THREE.Mesh( geometry, material );
		axis_3d.add( cylinder );
		cylinder.position.set( 0, scale / 2, 0 );
		cylinder.scale.set( scale,scale,scale );
		cylinder.rotation.set( 0, 0, 0 );
		// Z axis
		material = new THREE.MeshBasicMaterial( {color: 0x0000ff} );
		cylinder = new THREE.Mesh( geometry, material );
		axis_3d.add( cylinder );
		cylinder.position.set( 0, 0, scale / 2 );
		cylinder.scale.set( scale,scale,scale );
		cylinder.rotation.set( Math.PI * 0.5, 0, 0 );

	material = new THREE.MeshBasicMaterial( { vertexColors: THREE.FaceColors, overdraw: 0.5 } );

	geometry = new THREE.BoxGeometry( 0.02, 0.02, 0.02 );
	floating_cube = new THREE.Mesh( geometry, material );
	scene.add( floating_cube );

	geometry = new THREE.BoxGeometry( 0.09, 0.02, 0.09 );
	righthand_cube = new THREE.Mesh( geometry, material );
	scene.add( righthand_cube );

	geometry = new THREE.BoxGeometry( 0.02, 0.02, 0.02 );
	leftfoot_ik_cube = new THREE.Mesh( geometry, material );
	leftfoot_ik_cube.add( axis_3d.clone() );
	leftfoot_ik_cube.rotateOnAxis ( new THREE.Vector3(0,0,1), Math.PI * 1.2 );
	leftfoot_ik_cube.rotateOnAxis ( new THREE.Vector3(1,0,0), Math.PI * 0.05 );
	scene.add( leftfoot_ik_cube );

	rightfoot_ik_cube = new THREE.Mesh( geometry, material );
	scene.add( rightfoot_ik_cube );

	righthand_ik_cube = new THREE.Mesh( geometry, material );
	righthand_ik_cube.add( axis_3d.clone() );
	scene.add( righthand_ik_cube );

	//track_weight_target = 1;
	track_head = new THREE.Object3D();
	scene.add( track_head );

	// avatar
	avatar = new SysiphusAvatar();
	avatar.ready_callbak = on_avatar_ready;
	avatar.configure( config_sysiphus_avatar );
	avatar.load( scene );

	gridHelper.position.set( 0, 1, 0 );

	// events
	window.addEventListener( 'resize', onWindowResize, false );
	// container.addEventListener( 'mousedown', on_mouse_down, false );
	// container.addEventListener( 'click', on_mouse_up, false );

}

function on_avatar_ready() {

	avatar.move( 0, 1, 0 );
	// axis_3d_free.position.set( 0, 1.5, 0 );
	console.log( avatar.obj3d_mesh.material );
	avatar.obj3d_mesh.material.color = new THREE.Color( 0x000000 );
	avatar.obj3d_mesh.material.specular = new THREE.Color( 0x660033 );
	avatar.obj3d_mesh.material.emissive = new THREE.Color( 0x220022 );
	avatar.obj3d_mesh.material.shininess = 2;
	avatar.obj3d_mesh.material.reflectivity = 0;
	// avatar.obj3d_mesh.material.vertexColors = new THREE.Color( 0xff00ff );
	// avatar.obj3d_mesh.material.wireframe = true;
	// avatar.obj3d_mesh.visible = false;
	// initialise track_head_targetss
	track_head_targets = {};
	for ( var i in track_head_list ) {
		var name = track_head_list[i];
		if ( name == "camera" ) {
			track_head_targets[name] = [ camera.position, 0 ];
		} else if ( name == "floating_cube" ) {
			track_head_targets[name] = [ floating_cube.position, 0 ];
		} else if ( name == "left_foot" ) {
			track_head_targets[name] = [ avatar.bones["mixamorig_LeftHand"].world_position, 0 ];
		} else if ( name == "right_foot" ) {
			track_head_targets[name] = [ avatar.bones["mixamorig_RightFoot"].world_position, 0 ];
		} else {
			track_head_targets[name] = [ undefined, 0 ];
		}
	}

	leftfoot_ik_cube.position.set( 0.3, 1.44, 0.21 );
	righthand_ik_cube.position.set( -0.46, 2.21, -0.14 );

	avatar.ik( "mixamorig_LeftFoot", leftfoot_ik_cube, ik_weight_current );
	avatar.ik( "mixamorig_RightHand", righthand_ik_cube, ik_weight_current );
	// avatar.ik( "mixamorig_RightFoot", rightfoot_ik_cube, 0 );
	// avatar.ik( "mixamorig_LeftFoot", leftfoot_ik_cube, 1 );
	// avatar.bones["mixamorig_Hips"].bone.add( axis_3d.clone() );
	// avatar.bones["mixamorig_RightUpLeg"].bone.add( axis_3d.clone() );
	// avatar.bones["mixamorig_RightLeg"].bone.add( axis_3d.clone() );
	// avatar.bones["mixamorig_RightFoot"].bone.add( axis_3d.clone() );
	// avatar.bones["mixamorig_LeftUpLeg"].bone.add( axis_3d.clone() );
	avatar.bones["mixamorig_RightArm"].bone.add( axis_3d.clone() );
	// avatar.bones["mixamorig_LeftLeg"].bone.add( axis_3d.clone() );
	// avatar.bones["mixamorig_LeftFoot"].bone.add( axis_3d.clone() );
	// avatar.bones["Armature_breathing"].bone.add( axis_3d.clone() );
	populate_info();

	var panel = new dat.GUI( { width: 400 } );
	ui_settings = {
		'right hand pos x': righthand_ik_cube.position.x,
		'right hand pos y': righthand_ik_cube.position.y,
		'right hand pos z': righthand_ik_cube.position.z,
		'right hand rot x': righthand_ik_cube.rotation.x,
		'right hand rot y': righthand_ik_cube.rotation.y,
		'right hand rot z': righthand_ik_cube.rotation.z,
		'ik weight': ik_weight_target,
	};
	var folder_ik = panel.addFolder( 'IK ' );
	folder_ik.add( ui_settings, 'right hand pos x', -4.0, 0, 0.01 ).listen().onChange( function ( v ) { righthand_ik_cube.position.x = v; } );
	folder_ik.add( ui_settings, 'right hand pos y', -0.5, 4.0, 0.01 ).listen().onChange( function ( v ) { righthand_ik_cube.position.y = v; } );
	folder_ik.add( ui_settings, 'right hand pos z', -0.5, 4.0, 0.01 ).listen().onChange( function ( v ) { righthand_ik_cube.position.z = v; } );
	folder_ik.add( ui_settings, 'right hand rot x', -Math.PI, Math.PI, 0.01 ).listen().onChange( function ( v ) { righthand_ik_cube.rotation.x = v; } );
	folder_ik.add( ui_settings, 'right hand rot y', -Math.PI, Math.PI, 0.01 ).listen().onChange( function ( v ) { righthand_ik_cube.rotation.y = v; } );
	folder_ik.add( ui_settings, 'right hand rot z', -Math.PI, Math.PI, 0.01 ).listen().onChange( function ( v ) { righthand_ik_cube.rotation.z = v; } );
	folder_ik.add( ui_settings, 'ik weight', 0, 1, 0.01 ).listen().onChange( function ( v ) { ik_weight_target = v; } );
	folder_ik.open();

}

function number_display( num ) {

	var str = "" + Math.floor( num * 100 ) / 100;
	var n = str.indexOf(".");
	if ( n == -1 ) {
		return str + ".00";
	} else if  ( n == str.length - 2 ) {
		return str + "0";
	}
	return str;

}

function populate_info() {

	var row;
	var cell;

	var info_animations = document.getElementById( 'info_animations' );
	var info_animations_body = document.getElementById( 'info_animations_body' );
	for ( var i = 0; i < avatar.animations.length; ++i ) {
		var j = 0;
		var weight = avatar.mixer.clipAction( avatar.animations[i] ).getEffectiveWeight();
		// adding a row
		row = info_animations_body.insertRow( info_animations_body.rows.length );
		row.id = "info_" + avatar.animations[i].name;
		// adding cells
		// name
		cell = row.insertCell(j); j++;
		cell.className = "name first";
		cell.appendChild(document.createTextNode( avatar.animations[i].name ));
		// duration
		cell = row.insertCell(j); j++;
		cell.className = "duration";
		cell.appendChild(document.createTextNode( number_display( avatar.animations[i].duration ) ));
		// time
		cell = row.insertCell(j); j++;
		cell.id = "info_time_" + avatar.animations[i].name;
		cell.className = "time";
		cell.appendChild(document.createTextNode( number_display( avatar.mixer.clipAction( avatar.animations[i] ).time ) ));
		// speed
		cell = row.insertCell(j); j++;
		cell.id = "info_speed_" + avatar.animations[i].name;
		cell.className = "speed";
		cell.appendChild(document.createTextNode( number_display( avatar.mixer.clipAction( avatar.animations[i] ).timeScale ) ));
		// weight
		cell = row.insertCell(j); j++;
		cell.id = "info_weight_" + avatar.animations[i].name;
		cell.className = "weight";
		cell.appendChild(document.createTextNode( number_display( weight ) ));
		// loop display
		cell = row.insertCell(j); j++;
		cell.id = "info_looped_" + avatar.animations[i].name;
		cell.className = "looped";
		var checkbox = document.createElement('div');
		checkbox.className = "looped_checkbox";
		checkbox.id = "info_looped_checkbox_" + avatar.animations[i].name;
		cell.appendChild( checkbox );
		// checkbox
		cell = row.insertCell(j); j++;
		cell.id = "info_radio_" + avatar.animations[i].name;
		cell.className = "radio last";
		var checkbox = document.createElement('input');
		checkbox.type = "radio";
		checkbox.name = "radio_animation_selector";
		checkbox.id = "radio_" + avatar.animations[i].name;
		checkbox.value = avatar.animations[i].name;
		checkbox.className = "radio_animation";
		checkbox.addEventListener( "click", function(){ avatar.request_anim = this.value; } );
		if ( avatar.animations[i].name == avatar.request_anim ) {
			checkbox.checked = true;
		}
		cell.appendChild( checkbox ) ;
	}

	var info_tracking = document.getElementById( 'info_tracking_head' );
	var info_tracking_body = document.getElementById( 'info_tracking_head_body' );
	for ( var i in track_head_list ) {
		var name = track_head_list[i];
		var j = 0;
		// adding a row
		row = info_tracking_body.insertRow(info_tracking_body.rows.length);
		row.id = "info_tracking_" + name;
		// adding cells
		// name
		cell = row.insertCell(j); j++;
		cell.className = "name first";
		cell.appendChild(document.createTextNode( name ));
		// weight
		cell = row.insertCell(j); j++;
		cell.id = "info_tracking_weight_" + name;
		cell.className = "weight";
		cell.appendChild(document.createTextNode( number_display( track_head_targets[name][1] ) ));
		// checkbox
		cell = row.insertCell(j); j++;
		cell.id = "info_tracking_head_" + name;
		cell.className = "radio last";
		var checkbox = document.createElement('input');
		checkbox.type = "radio";
		checkbox.name = "radio_tracking_head_selector";
		checkbox.id = "radio_" + name;
		checkbox.value = name;
		checkbox.className = "radio_animation";
		checkbox.addEventListener( "click", function(){ set_track_head( this.value ); } );
		if ( name == "__NONE__" ) {
			checkbox.checked = true;
		}
		cell.appendChild( checkbox ) ;
	}

	var info_scales = document.getElementById( 'info_scale_presets' );
	var info_scales_body = document.getElementById( 'info_scale_presets_body' );
	for ( var name in avatar.deforms ) {
		var j = 0;
		// adding a row
		row = info_scales_body.insertRow(info_scales_body.rows.length);
		row.id = "info_scale_presets_" + name;
		// adding cells
		// name
		cell = row.insertCell(j); j++;
		cell.className = "name first";
		cell.appendChild(document.createTextNode( name ));
		// checkbox
		cell = row.insertCell(j); j++;
		cell.id = "info_scale_presets_" + name;
		cell.className = "radio last";
		var checkbox = document.createElement('input');
		checkbox.type = "radio";
		checkbox.name = "radio_scale_presets_selector";
		checkbox.id = "radio_" + name;
		checkbox.value = name;
		checkbox.className = "radio_animation";
		checkbox.addEventListener( "click", function(){ avatar.goto_deform( this.value ); } );
		if ( name == "__UNIT__" ) {
			checkbox.checked = true;
		}
		cell.appendChild( checkbox ) ;
	}

	print( "bones" );
	for( var name in avatar.bones ) {
		print( "&nbsp;&nbsp;&nbsp;&nbsp;" + avatar.bones[name].bone.name );
	}

}

function update_info() {

	if ( avatar.ready === true ) {
		for ( var i = 0; i < avatar.animations.length; ++i ) {
			var aname = avatar.animations[i].name;
			var track = avatar.mixer.clipAction( avatar.animations[i] );
			document.getElementById( "info_weight_" + aname ).innerHTML =
				number_display( track.getEffectiveWeight() );
			document.getElementById( "info_speed_" + aname ).innerHTML =
				number_display( track.getEffectiveTimeScale() );
			document.getElementById( "info_time_" + aname ).innerHTML =
				number_display( track.time );
			var cb = document.getElementById( "info_looped_checkbox_" + aname );
			if ( track.just_looped && cb.className != "looped_checkbox checked" ) {
				cb.className = "looped_checkbox checked";
			} else if ( cb.className == "looped_checkbox checked" ) {
				cb.className = "looped_checkbox normal";
			}
		}
		for ( var name in track_head_targets ) {
			document.getElementById( "info_tracking_weight_" + name ).innerHTML =
				number_display( track_head_targets[name][1] );
		}
	}

}

function onWindowResize() {

	camera.aspect = window.innerWidth / window.innerHeight;
	camera.updateProjectionMatrix();
	renderer.setSize( window.innerWidth, window.innerHeight );

}

function animate() {

	requestAnimationFrame( animate );
	render();
	if ( stats_enabled ) {
		stats.update();
	}

}

function render() {

	var delta = clock.getDelta();

	floating_cube.position.set(
			Math.cos( frame_count * 0.02 ),
			2.5 + 0.3 * Math.cos( frame_count * 0.032 ),
			Math.sin( frame_count * 0.02 ) );

	var speed = 1;
	var angl = Math.PI * 0.5;
	leftfoot_ik_cube.position.set(
		0.2 + Math.cos( angl + frame_count * 0.02 * speed ) * 0.03,
		1.5 + Math.sin( angl + frame_count * 0.045 * speed ) * 0.2,
		Math.sin( angl + frame_count * 0.02 * speed ) * 0.4 );
	floating_cube.updateMatrix();

	var track_headp = track_head.position;
	for ( var name in track_head_targets ) {
		var target = track_head_targets[name];
		if ( track_head_current == name && target[1] < 1 ) {
			target[1] += delta;
			if ( target[1] > 1 ) {
				target[1] = 1;
			}
		} else if ( track_head_current != name && target[1] > 0 ) {
			target[1] -= delta;
			if ( target[1] < 0 ) {
				target[1] = 0;
			}
		}
		if ( target[0] !== undefined ) {
			track_headp.lerp( target[0], target[1] );
		}
	}
	// track_head.position.copy(track_headp);
	track_head.updateMatrix();

	if ( avatar.ready === true ) {

		if ( ik_weight_current != ik_weight_target ) {
			if ( ik_weight_current < ik_weight_target ) {
				ik_weight_current += delta * 1.5;
				if ( ik_weight_current > ik_weight_target ) {
					ik_weight_current = ik_weight_target;
				}
			} else {
				ik_weight_current -= delta * 1.5;
				if ( ik_weight_current < ik_weight_target ) {
					ik_weight_current = ik_weight_target;
				}
			}
			var w = Interpolation.sin( ik_weight_current );
			avatar.ik( "mixamorig_RightHand", righthand_ik_cube, w );
			avatar.ik( "mixamorig_LeftFoot", leftfoot_ik_cube, w );
		}

		if ( track_weight_current != track_weight_target ) {

			if ( track_weight_current < track_weight_target ) {
				track_weight_current += delta * track_weight_speed;
				if ( track_weight_current > track_weight_target ) {
					track_weight_current = track_weight_target;
				}
			} else {
				track_weight_current -= delta * track_weight_speed;
				if ( track_weight_current < track_weight_target ) {
					track_weight_current = track_weight_target;
				}
			}

			if ( track_weight_current == 0 ) {

				avatar.track_to( "mixamorig_Spine2", undefined );
				avatar.track_to( "mixamorig_Neck", undefined );
				avatar.track_to( "mixamorig_Head", undefined );

			} else {

				var alpha = Interpolation.sin( track_weight_current );
				avatar.track_to( "mixamorig_Spine2", track_head, alpha * 0.4 );
				avatar.track_to( "mixamorig_Neck", track_head, alpha * 0.7 );
				avatar.track_to( "mixamorig_Head", track_head, alpha );

			}

		}

		avatar.update( delta );

		righthand_cube.position.copy( avatar.bones["mixamorig_LeftHand"].world_position );
		righthand_cube.quaternion.copy( avatar.bones["mixamorig_LeftHand"].world_quaternion );

		// bone_ik_dir.position.copy( avatar.bones["mixamorig_RightUpLeg"].world_position );
		// bone_ik_dir.quaternion.copy( avatar.bones["mixamorig_RightUpLeg"].world_quaternion );
		// bone_ik_dir.position.copy( avatar.bones["mixamorig_RightLeg"].world_position );
		// bone_ik_dir.quaternion.copy( avatar.bones["mixamorig_RightLeg"].world_quaternion );


	}

	renderer.render( scene, camera );

	update_info();

	frame_count++;

}

function set_track_head( name ) {

	if ( name == track_head_list[0] ) {
		track_weight_target = 0;
	} else {
		track_weight_target = 1;
	}
	track_head_current = name;

}

function print( e ) {

	document.getElementById("debugger").innerHTML += e;
	document.getElementById("debugger").innerHTML += "<br/>";

}

function debug( e ) {

	document.getElementById("debugger").innerHTML = e;

}

function debug_json( e ) {

	document.getElementById("debugger").innerHTML = JSON.stringify(e, null, 4);

}
