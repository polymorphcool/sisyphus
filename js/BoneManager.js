/*
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      

 libre video game bazaar

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of sysiphus project
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2018 polymorph.cool
 All rights reserved. 
 
 MIT License

 Permission is hereby granted, free of charge, to any person obtaining a copy of this
 software and associated documentation files (the "Software"), to deal in the Software 
 without restriction, including without limitation the rights to use, copy, modify, merge,
 publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 to whom the Software is furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED *AS IS*, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.
 
 ___________________________________( ^3^)_____________________________________
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
*/

/* requires BoneController.js to be loaded
 * object passed to nit method must be an armature, with a 'children' hierarchy
 */

function BoneManager() {

	this.ctrls = undefined;

	// SCALE management
	// hashmap of presets
	this.presets = undefined;

	// used during loading
	this.preset_path = undefined;
	this.preset_loading_callback = undefined;
	this.preset_loaded_callback = undefined;

	// used for interpolations
	this.preset_lerp_alpha = 0;
	this.preset_default = undefined;
	this.preset_target = undefined;
	this.preset_current = undefined;

	this.init = function( obj ) {
		this.ctrls = {};
		this.presets = {};
		this.preset_path = [];
		this.bone_load( obj );

		this.preset_default = new BoneScalePreset();
		this.preset_default.init( this.ctrls );
		this.presets[ this.preset_default.name ] = this.preset_default;

		this.preset_target = this.preset_default;
		this.preset_current = this.preset_default;

	};

	this.bone_load = function( obj, parent = undefined ) {
		var bc = new BoneController();
		bc.init( obj, parent );
		this.ctrls[ obj.name ] = bc;
		//print( bone_hierarchy[ child.name ].to_string() );
		for( var i = 0; i < obj.children.length; ++i ) {
			this.bone_load( obj.children[i], bc );
		}
	};

	this.loading_callback = function( cb ) {
		this.preset_loading_callback = cb;
	};

	this.loaded_callback = function( cb ) {
		this.preset_loaded_callback = cb;
	};

	this.load_presets = function( list ) {
		// launching the presets loading
		// copying the list
		for( var i = 0; i < list.length; ++i ) {
			this.preset_path.push( list[i] );
		}
		// launching the listt parsing
		this.on_preset_loaded();
	};

	this.on_preset_loaded = function() {
		if ( this.preset_path.length == 0 ) {
			if( this.preset_loaded_callback !== undefined ) {
				this.preset_loaded_callback();
			}
			return;
		}
		var path = this.preset_path.shift();
		var xobj = new XMLHttpRequest();
		xobj.bone_mgr = this;
		xobj.overrideMimeType("application/json");
		xobj.open('GET', path, true);
		this.presets_loading( path );
		xobj.onreadystatechange = function () {
			// console.log( xobj );
			if (xobj.readyState == 4 && xobj.status == "200") {
            	// Required use of an anonymous callback as .open will NOT return a value but simply returns undefined in asynchronous mode
            	this.bone_mgr.json_load_success(xobj.responseText);
          	} else if ( xobj.status != "200" ) {
          		this.bone_mgr.json_load_failed(xobj);
          	}
		}
		xobj.send(null);
	};

	this.json_load_success = function( e ) {
		try{ 
			var data = JSON.parse( e );
			var bsp = new BoneScalePreset();
			bsp.init( this.ctrls, data );
			this.presets[ bsp.name ] = bsp;
		} catch( exception ) {
			console.log( "parsing of " + e + " failed, \n" + exception );
		}
		this.on_preset_loaded();
	};

	this.json_load_failed = function( e ) {
		console.log( "loading of " + e + " failed" );
		this.on_preset_loaded();
	};

	this.presets_loading = function( path ) {
		if ( this.preset_loading_callback !== undefined ) {
			// callback.call( this.preset_loading_callback( path ) );
			this.preset_loading_callback( path );
		}
	};

	this.update = function( delta ) {
		var do_lerp = false;
		if ( this.preset_current != this.preset_target ) {
			this.preset_lerp_alpha += delta;
			do_lerp = true;
			if ( this.preset_lerp_alpha >= 1 ) {
				this.preset_lerp_alpha = 1;
			}
		}
		for( var name in this.ctrls ) {
			if ( do_lerp ) {
				this.ctrls[name].set( 
					this.preset_target.lerp( 
						this.preset_current, name, 
						Interpolation.sin( this.preset_lerp_alpha ) 
						)
					);
			}
			this.ctrls[name].update();
		}
		if ( this.preset_lerp_alpha >= 1 ) {
			this.preset_lerp_alpha = 0;
			this.preset_current = this.preset_target;
			// console.log( "stopping lerp " + this.preset_current.name );
		}
	}

	this.goto_preset = function( name ) {
		if ( name == this.preset_target.name ) {
			// console.log( "preset " + name + " is already loaded" );
			return;
		}
		this.preset_target = this.presets[name];
		this.preset_lerp_alpha = 0;
	};

	this.length = function( name, l ) {
		this.ctrls[name].length( l );
	};

	this.radii = function( name, rx, rz = undefined ) {
		this.ctrls[name].radii( rx, rz );
	};

	this.scale = function( name, x, y = undefined, z = undefined ) {
		if ( y === undefined ) {
			y = x;
		}
		this.ctrls[name].length( y );
		this.ctrls[name].radii( x, z );
	};

	this.track_to = function( name, obj3d, alpha ) {

		var bone = this.ctrls[name].bone;
		var bone_wpos = new THREE.Vector3();
		var bone_wquat = new THREE.Quaternion();
		var bone_wscale = new THREE.Vector3();
		bone.matrixWorld.decompose( bone_wpos, bone_wquat, bone_wscale );
		var bone_lpos = new THREE.Vector3();
		var bone_lquat = new THREE.Quaternion();
		var bone_lscale = new THREE.Vector3();
		bone.matrix.decompose( bone_lpos, bone_lquat, bone_lscale );
		var bone_wmat3 = new THREE.Matrix3().setFromMatrix4( bone.matrixWorld );

		// debug
		// debug_cube.scale.x = 0.05;
		// debug_cube.scale.y = 0.3;
		// debug_cube.position.copy(bone_wpos);
		// debug_cube.rotation.copy(new THREE.Euler().setFromQuaternion( bone_wquat ));

		var tracked_wpos = new THREE.Vector3();
		var tracked_wquat = new THREE.Quaternion();
		var tracked_wscale = new THREE.Vector3();
		obj3d.matrixWorld.decompose( tracked_wpos, tracked_wquat, tracked_wscale );
		var tracked_wmat3 = new THREE.Matrix3().setFromMatrix4( obj3d.matrixWorld );

		var bone_front = new THREE.Vector3(0,0,1).applyMatrix3( bone_wmat3 );
		var obj3d_front = new THREE.Vector3(0,0,-1).applyMatrix3( tracked_wmat3 );

		var delta_wpos = new THREE.Vector3().subVectors( tracked_wpos, bone_wpos ).normalize();

		var empty = new THREE.Quaternion();
		var new_lmat = new THREE.Matrix4().compose( tracked_wpos, tracked_wquat.slerp( empty, 1 - alpha ), tracked_wscale );
		if ( bone.parent != undefined ) {
			var parent_mat = new THREE.Matrix4().getInverse(bone.parent.matrixWorld);
			parent_mat.multiply(new_lmat);
			new_lmat = parent_mat;
		}

		var new_lpos = new THREE.Vector3();
		var new_lquat = new THREE.Quaternion();
		var new_lscale = new THREE.Vector3();
		new_lmat.decompose( new_lpos, new_lquat, new_lscale );

		var slerped = bone_lquat.clone();
		slerped.slerp( new_lquat, alpha );

		bone.rotation.copy(new THREE.Euler().setFromQuaternion( slerped ));
		//bone.quaternion.slerp( new_lquat, 0.01 );

	};

	this.to_string = function() {
		var out = "";
		var bnum = 0;
		for( var name in this.ctrls ) {
			++bnum;
			out += "&nbsp;&nbsp;&nbsp;&nbsp;" + name + "<br/>";
		}
		var out = "BoneManager, " + bnum + " bone(s)<br/>" + out;
		return out;
	};

};