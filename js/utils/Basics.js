/*

 _________ ____  .-. _________/ ____ .-. ____
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'

 libre video game bazaar

 ____________________________________  ?   ____________________________________
                                     (._.)


 This file is part of sysiphus project
 For the latest info, see http://polymorph.cool/

 Copyright (c) 2018 polymorph.cool
 All rights reserved.

 MIT License

 Permission is hereby granted, free of charge, to any person obtaining a copy of this
 software and associated documentation files (the "Software"), to deal in the Software
 without restriction, including without limitation the rights to use, copy, modify, merge,
 publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED *AS IS*, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.

 ___________________________________( ^3^)_____________________________________

 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)

*/

function Basics() {};

Basics.debug_axis = function() {

 	var axis_3d = new THREE.Object3D();
	// axis
	var cylinder;
	var scale = 0.3;
	// X axis
	geometry = new THREE.CylinderGeometry( 0.015, 0.015, 1, 3 );
	material = new THREE.MeshBasicMaterial( {color: 0xff0000, depthTest:false} );
	cylinder = new THREE.Mesh( geometry, material );
	axis_3d.add( cylinder );
	cylinder.position.set( scale / 2, 0, 0 );
	cylinder.scale.set( scale,scale,scale );
	cylinder.rotation.set( 0, 0, Math.PI * 0.5 );
	// Y axis
	material = new THREE.MeshBasicMaterial( {color: 0x00ff00} );
	cylinder = new THREE.Mesh( geometry, material );
	axis_3d.add( cylinder );
	cylinder.position.set( 0, scale / 2, 0 );
	cylinder.scale.set( scale,scale,scale );
	cylinder.rotation.set( 0, 0, 0 );
	// Z axis
	material = new THREE.MeshBasicMaterial( {color: 0x0000ff} );
	cylinder = new THREE.Mesh( geometry, material );
	axis_3d.add( cylinder );
	cylinder.position.set( 0, 0, scale / 2 );
	cylinder.scale.set( scale,scale,scale );
	cylinder.rotation.set( Math.PI * 0.5, 0, 0 );

	return axis_3d;

};

Basics.clone = function(obj) {
    if (null == obj || "object" != typeof obj) return obj;
    var copy = obj.constructor();
    for (var attr in obj) {
        if (obj.hasOwnProperty(attr)) copy[attr] = Basics.clone(obj[attr]);
    }
    return copy;
};

Basics.nice_date = function() {
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!
	var yyyy = today.getFullYear();
	if(dd<10) {
	  dd = '0'+dd;
	}
	if(mm<10) {
	  mm = '0'+mm;
	}
	return yyyy + "." + mm + '.' + dd;
};

Basics.nice_time = function( millis ) {
  var secs = Math.floor( millis / 1000 );
  var d = Math.floor( secs / ( 3600 * 24 ) );
  secs -= d * ( 3600 * 24 );
  var h = Math.floor( secs / 3600 );
  secs -= h * 3600;
  var m = Math.floor( secs / 60 );
  secs -= m * 60;
  return d + "-" + h + "-" + m + "-" + Math.floor( secs );
}

Basics.nice_millis = function( millis ) {

	var secs = Math.floor( millis / 1000 );
	millis -= secs * 1000;
	var mins = Math.floor( secs / 60 );
	secs -= mins * 60;
	var hours = Math.floor( mins / 60 );
	mins -= hours * 60;
	var days = Math.floor( hours / 24 );
	hours -= days * 24;
	var out = "";
	if ( days > 0 ) {
		out += days + " day";
		if ( days > 1 ) { out += "s"; }
		out += " ";
	}
	if ( hours > 0 ) {
		out += hours + " hour";
		if ( hours > 1 ) {	out += "s"; }
		out += " ";
	}
	if ( mins > 0 ) {
		out += mins + " minutes";
		if ( mins > 1 ) {	out += "s"; }
		out += " ";
	}
	if ( secs > 0 ) {
		out += secs + "′ ";
	}
	if ( millis > 0 ) {
		out += millis + "′′ ";
	}
	return out;

};

Basics.prototype.func = function() {
    this.constructor.staticMethod();
}
