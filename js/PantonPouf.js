/*

 _________ ____  .-. _________/ ____ .-. ____
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'

 libre video game bazaar

 ____________________________________  ?   ____________________________________
                                     (._.)


 This file is part of sysiphus project
 For the latest info, see http://polymorph.cool/

 Copyright (c) 2018 polymorph.cool
 All rights reserved.

 MIT License

 Permission is hereby granted, free of charge, to any person obtaining a copy of this
 software and associated documentation files (the "Software"), to deal in the Software
 without restriction, including without limitation the rights to use, copy, modify, merge,
 publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED *AS IS*, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.

 ___________________________________( ^3^)_____________________________________

 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)

*/

/*
 Learning a bit of design history by reading the comments.


    ;#@@@@@@@@@@@@@'`
   @@@@@######@#@@@@@@@#
   @@@@@##########@@@@@@@@`
   @@@#+###########@@@@@@@@#
  `@@@#'+@##########@@@@@@@@#,
  `@@#+;+#############@@@@@@@#'
  .@@#;;'##++##########@@@@@@@#;
  +@@#:;'##++###########@@@@####,
  @@#+::;##+++##########@;,,,;###
  @@#+;:;##+++############,,,.,,'#
  @@#'@:;#++++############,,....,+.
 `@@#:@:'#++++############',,....,#
 ;@#+'@:'#++++###########@#,,.....,:
 +@##'@:'#++++###########@#,,......#
 #@##+@:'#++++#########@@@@,,,......'
 +@@##@#;##+++#########@@@#,,,,,,:+@#`
 .@@###@#+##+#########@@@@#+@@@@@@@@@#.
  @@@#@@@@#@######@@@@@@@@@@@@@@@@@@@@@;;::::::,,,,,,:::,.
   #@@@@@@@@@@@@#@##########'::;'+###@@,,.....`..,,::;######+.
     ;+##########################;::::;;;'+####################.
                       :################+++++++#################'
                          +########++++''''##++++################:
                           .@######+'''+++'''''++++++++##+:;+####+
                            `@@##,,+'+#######@##+++++++++''###:'##`
                             +@@@#,++##@@@@@@@@@++++++++++++'+##;;.
                              #@@@'++@@@@@@@@@@#+++++++++++++''###.
                              #@@@+##@@@@@@@@@@#####+##+++++++'###
                              ;@@@#:@@@@@@@@@##########++++++++##+
                              `@@@#+@@@@@@###@@########++++++++++`
                              `####@@@@#@@@@@@@##########++++++''
                             .,++#+##@@@@@@@@@@@#########++++++'
                           .,,:++#+@@@@@@@@@@@@@@@#########++'+.
                        `.,,.`'++#@@@@@@@@@@@@@@@@@@@@@@@@@+++:
                     `.,,,.`;@++++@@@@@@@@@@@@@@@@@@@@@@@@@++;
                  `.,,,,.`;@@@++'#@@@@@@@@@@@@@@@@@@@@@@@@#+;
              `.,,,,,.`,#@@@#++++@@@@@@@@@@@@@@@@@@@@@@@@#+;
         `.,,,:,,,.,;@@@@@#+++++#@@@@@@@@@@@@@@@@@@@@@@@@+;
        ,,:,,,:'#@@@@@@##+++++++    .,;'+@@@@@@@@@@@@@@@+:
        ,,#@@@#@@@@@#++++++++++`              `,:'+@@@@#,
          `:+####++++++++++++++
               `,;+##+++++#+++
                      .:'+#++;
                           ,,

 'The Iconic "S Chair" pictured above, developed by Danish designer Verner Panton in the late
  1960s, is a good example of the way in which fiberglass and improved plastics technology
  changed the way designers thought about furniture.'
  (ref: http://www.modrestoration.com/technology-and-mid-century-furniture-design)
  Ascii art generated with http://picascii.com.
  See also https://en.wikipedia.org/wiki/Panton_Chair

*/

function PantonPoufConfig() {

	this.morph_path = undefined;
	this.morph_default = undefined;
	this.butt_anim_path = undefined;
	this.falling_anim_path = undefined;

	this.weight_init = 0;
	this.weight_speed = 1;
	this.weight_method = Interpolation.sin;
	this.default_visibility = true;

	this.left_foot_anchor = undefined;
	this.right_foot_anchor = undefined;

	this.debug_anchors = true;
	this.ready_callbak = undefined;

	this.diffuse_map = undefined;
	this.specular_map = undefined;
	this.normal_map = undefined;

	this.clone = function() {

		var out = new PantonPoufConfig();
		out.morph_path = this.morph_path;
		out.morph_default = this.morph_default;
		out.butt_anim_path = this.butt_anim_path;
		out.falling_anim_path = this.falling_anim_path;
		out.weight_init = this.weight_init;
		out.weight_speed = this.weight_speed;
		out.weight_method = this.weight_method;
		out.default_visibility = this.default_visibility;
		out.left_foot_anchor = this.left_foot_anchor;
		out.right_foot_anchor = this.right_foot_anchor;
		out.ready_callbak = this.ready_callbak;
		out.debug_anchors = this.debug_anchors;
		out.diffuse_map = this.diffuse_map;
		out.specular_map = this.specular_map;
		out.normal_map = this.normal_map;
		return out;
	};

};

function PantonPouf() {

	this.config = undefined;

	this.morphable = undefined;
	this.pouf = undefined;
	this.scene = undefined;
	this.butt_root = undefined;
	this.butt = undefined;
	this.butt_mixer = undefined;
	this.butt_animations = undefined;

	this.left_foot = undefined;
	this.right_foot = undefined;

	this.ready = false;
	this.ready_callbak = undefined;

	this.morph_weights = {};

	this.request_morph = undefined;

	this.pouf_mixer = undefined;
	this.pouf_animations = undefined;
	this.pouf_is_falling = false;

	// utilities

	this.visible = function( enable ) {

		if ( this.ready === false ) return;
		this.pouf.visible = enable;
		this.morphable.obj3d.visible = enable;

	};

	this.move = function( x, y, z ) {

		if ( this.ready === false ) return;
		this.pouf.position.set(x, y, z);
		this.butt_root.position.set(x, y, z);

	};

	this.scale = function( x, y, z ) {

		if ( this.ready === false ) return;
		this.pouf.scale.set(x, y, z);
		this.butt_root.scale.set(x, y, z);

	};

	this.start_falling = function() {

		if ( this.ready === false ) return;
		this.pouf_is_falling = true;
		this.pouf_animations["falling"].play();
		if ( !this.pouf.visible ) {
			this.visible( true );
		}

	};

	this.stop_falling = function() {

		if ( this.ready === false ) return;
		this.pouf_is_falling = false;
		this.pouf_animations["falling"].time = this.pouf_animations["falling"].duration;
		if ( !this.pouf.visible ) {
			this.visible( true );
		}

	};

	// loading and management

	this.configure = function( config ) {

		this.config = config.clone();
		this.morphable = new MorphableMesh();

	};

	this.load = function( scene ) {

		this.scene = scene;
		this.morphable.path = this.config.morph_path;
		this.morphable.scene = this.scene;
		this.morphable.default_visibility = this.config.default_visibility;
		this.morphable.listener = this;
		this.morphable.load();

	};

	this.morphablemesh_loaded = function() {

		if ( this.morphable.obj3d !== undefined ) {

			this.pouf = new THREE.Object3D();
			this.scene.add( this.pouf );
			this.morphable.obj3d.parent = this.pouf;
			// this.morphable.obj3d.rotateY( Math.PI * 0.5 );
			for ( var k in this.morphable.obj3d.morphTargetDictionary ) {
				this.morph_weights[ k ] = Interpolation.new_interpolation(
					k,
					this.config.weight_init,
					this.config.weight_speed,
					this.config.weight_method
				);
				if ( k == this.config.default_morph ) {
					this.morph_weights[ k ].set( 1 );
				}
			}

			// starting textures loading
			if (
				this.config.diffuse_map !== undefined ||
				this.config.normal_map !== undefined
			) {

				var textureLoader = new THREE.TextureLoader();
				var mat = new THREE.MeshPhongMaterial( {
					morphTargets: this.morphable.obj3d.material.morphTargets,
					morphNormals: this.morphable.obj3d.material.morphNormals,
					skinning: this.morphable.obj3d.material.skinning,
					color: 0xffffff,
					specular: 0xdddddd,
					shininess: 0
					// map: textureLoader.load( this.config.diffuse_map ),
					// specularMap: textureLoader.load( "obj/leeperrysmith/Map-SPEC.jpg" ),
					// normalMap: textureLoader.load( this.config.normal_map ),
					// normalScale: new THREE.Vector2( 2,2 )
				} );
				if ( this.config.diffuse_map !== undefined ) {
					mat.map = textureLoader.load( this.config.diffuse_map );
				}
				if ( this.config.diffuse_map !== undefined ) {
					mat.specularMap = textureLoader.load( this.config.specular_map );
				}
				if ( this.config.normal_map !== undefined ) {
					mat.normalMap = textureLoader.load( this.config.normal_map );
					mat.normalScale = new THREE.Vector2( 2,2 );
				}
				this.morphable.obj3d.material = mat;
				this.morphable.obj3d.material.needsUpdate = true;
				this.morphable.obj3d.castShadow = SHADOW_ENABLED;
				this.morphable.obj3d.receiveShadow = SHADOW_ENABLED;

			} else {

				var mat = new THREE.MeshNormalMaterial( {
					//lights: true,
					morphTargets: this.morphable.obj3d.material.morphTargets,
					morphNormals: this.morphable.obj3d.material.morphNormals
				} );
				this.morphable.obj3d.material = mat;

			}

			// loading animation
			this.load_falling_animations();
			// this.load_butt_animations();

		}

	};

	this.load_falling_animations = function() {

		var xobj = new XMLHttpRequest();
		xobj.caller = this;
		xobj.overrideMimeType("application/json");
		xobj.open('GET', this.config.falling_anim_path, true);
		xobj.onreadystatechange = function () {
			// console.log( xobj );
			if (xobj.readyState == 4 && xobj.status == "200") {
				// Required use of an anonymous callback as .open will NOT return a value but simply returns undefined in asynchronous mode
				this.caller.on_falling_animations_loaded(xobj.responseText);
			} else if ( xobj.status != "200" ) {
				this.caller.on_falling_animations_failed(xobj);
			}
		}
		xobj.send(null);

	};

	this.on_falling_loop = function( e ) {
		e.action.stop();
		this.pouf_is_falling = false;
	}

	this.on_falling_animations_loaded = function( e ) {

		try {

			var data = JSON.parse( e );

			this.pouf_mixer = new THREE.AnimationMixer( this.morphable.obj3d );
			this.pouf_mixer.caller = this;
			this.pouf_mixer.addEventListener( 'loop', function( e ) {
				this.caller.on_falling_loop(e);
			} );
			this.pouf_animations = {};

			var corrq = new THREE.Quaternion();
			corrq.setFromEuler( new THREE.Euler( Math.PI * 0.5,0,0, 'XYZ' ) );

			for ( var tname in data ) {
				var tracks = [];
				var max_duration = 0;
				for ( subt in data[tname] ) {
					// console.log( tname, subt );
					if ( subt == "location" ) {
						// turning into VectorKeyframeTrack
						var times = [];
						var values = [];
						for ( var frame in data[tname][subt] ) {
							var time = parseInt(frame);
							if ( max_duration < time ) {
								max_duration = time;
							}
							times.push( time );
							var vs = data[tname][subt][frame];
							values.push( vs[0] );
							values.push( vs[1] );
							values.push( vs[2] );
						}
						var t = new THREE.VectorKeyframeTrack( tname+".position", times, values );
						tracks.push( t );
					} else if ( subt == "rotation_euler" ) {
						var times = [];
						var values = [];
						for ( var frame in data[tname][subt] ) {
							var time = parseInt(frame);
							if ( max_duration < time ) {
								max_duration = time;
							}
							times.push( time );
							var vs = data[tname][subt][frame];
							var q = new THREE.Quaternion();
							q.setFromEuler( new THREE.Euler( vs[0], vs[1], vs[2], 'XYZ' ) );
							q = q.multiply(corrq);
							q.toArray( vs );
							values.push( vs[0] );
							values.push( vs[1] );
							values.push( vs[2] );
							values.push( vs[3] );
						}
						var t = new THREE.QuaternionKeyframeTrack( tname+".quaternion", times, values );
						tracks.push( t );
					}
				}

				var clip = new THREE.AnimationClip( tname, max_duration, tracks );
				var action = this.pouf_mixer.clipAction( clip );
				action.setEffectiveTimeScale(30);
				action.setEffectiveWeight(1);
				// action.play();
				this.pouf_animations[tname] = action;

			}

			// console.log( this.pouf_animations );
			// print( "PantonPouf: " + this.config.falling_anim_path + " loaded" );

			this.load_butt_animations();

		} catch( exception ) {

			console.log( "parsing of " + this.config.falling_anim_path + " failed, \n" + exception );
			this.on_butt_animations_failed();

		}

	};

	this.on_falling_animations_failed = function( e ) {
		console.log( "on_falling_animations_failed" );
		return;
	};

	this.load_butt_animations = function() {

		this.butt_root = new THREE.Object3D();
		this.scene.add( this.butt_root );
		this.butt = new THREE.Object3D();
		this.butt_root.add( this.butt );

		if( this.config.debug_anchors ) {
			this.butt.add( Basics.debug_axis() );
		}

		var xobj = new XMLHttpRequest();
		xobj.caller = this;
		xobj.overrideMimeType("application/json");
		xobj.open('GET', this.config.butt_anim_path, true);
		xobj.onreadystatechange = function () {
			// console.log( xobj );
			if (xobj.readyState == 4 && xobj.status == "200") {
            	// Required use of an anonymous callback as .open will NOT return a value but simply returns undefined in asynchronous mode
            	this.caller.on_butt_animations_loaded(xobj.responseText);
          	} else if ( xobj.status != "200" ) {
          		this.caller.on_butt_animations_failed(xobj);
          	}
		}
		xobj.send(null);

	};

	this.on_butt_animations_loaded = function( e ) {

		try {

			var data = JSON.parse( e );

			this.butt_mixer = new THREE.AnimationMixer(this.butt);
			this.butt_animations = {};

			for ( var tname in data ) {

				var tracks = [];
				var max_duration = 0;

				for ( subt in data[tname] ) {

					// console.log( tname, subt );

					if ( subt == "location" ) {

						// turning into VectorKeyframeTrack
						var times = [];
						var values = [];
						for ( var frame in data[tname][subt] ) {
							var time = parseInt(frame);
							if ( max_duration < time ) {
								max_duration = time;
							}
							times.push( time );
							var vs = data[tname][subt][frame];
							values.push( vs[0] );
							values.push( vs[1] );
							values.push( vs[2] );
						}
						var t = new THREE.VectorKeyframeTrack( tname+".position", times, values );
						tracks.push( t );

					} else if ( subt == "rotation_euler" ) {

						var times = [];
						var values = [];
						for ( var frame in data[tname][subt] ) {
							var time = parseInt(frame);
							if ( max_duration < time ) {
								max_duration = time;
							}
							times.push( time );
							var vs = data[tname][subt][frame];
							var q = new THREE.Quaternion();
							q.setFromEuler( new THREE.Euler( vs[0], vs[1], vs[2], 'XYZ' ) );
							q.toArray( vs );
							values.push( vs[0] );
							values.push( vs[1] );
							values.push( vs[2] );
							values.push( vs[3] );
						}
						var t = new THREE.QuaternionKeyframeTrack( tname+".quaternion", times, values );
						tracks.push( t );

					}

				}

				var clip = new THREE.AnimationClip( tname, max_duration, tracks );
				var action = this.butt_mixer.clipAction( clip );
				action.setEffectiveTimeScale(0);
				action.play();
				action.setEffectiveWeight(1);
				this.butt_animations[tname] = action;

			}

			// console.log( this.butt_animations );
			print( "PantonPouf: " + this.config.butt_anim_path + " loaded" );

			if ( this.pouf !== undefined ) {
				this.all_loaded();
			}

		} catch( exception ) {

			console.log( "parsing of " + this.path + " failed, \n" + exception );
			this.on_butt_animations_failed();

		}

	};

	this.on_butt_animations_failed = function() {
		return;
	};

	this.all_loaded = function() {

		this.ready = true;

		if ( this.config.left_foot_anchor !== undefined ) {
			this.left_foot = new THREE.Object3D();
			this.scene.add( this.left_foot );
			if( this.config.debug_anchors ) {
				this.left_foot.add( Basics.debug_axis() );
			}
			if ( this.config.left_foot_anchor["pos"] !== undefined ) {
				this.left_foot.position.copy( this.config.left_foot_anchor["pos"] );
			}
			if ( this.config.left_foot_anchor["euler"] !== undefined ) {
				this.left_foot.quaternion.setFromEuler( this.config.left_foot_anchor["euler"] );
			}
		}

		if ( this.config.right_foot_anchor !== undefined ) {
			this.right_foot = new THREE.Object3D();
			this.scene.add( this.right_foot );
			if( this.config.debug_anchors ) {
				this.right_foot.add( Basics.debug_axis() );
			}
			if ( this.config.right_foot_anchor["pos"] !== undefined ) {
				this.right_foot.position.copy( this.config.right_foot_anchor["pos"] );
			}
			if ( this.config.right_foot_anchor["euler"] !== undefined ) {
				this.right_foot.quaternion.setFromEuler( this.config.right_foot_anchor["euler"] );
			}
		}

		if ( this.config.ready_callbak !== undefined ) {
			this.config.ready_callbak();
		}

	};

	this.activate_state = function( name ) {
		this.request_morph = name;
	};

	this.get_morph_weight = function( name ) {
		return this.morph_weights[ name ].value;
	};

	this.set_influence = function( name, weight ) {
		
		if ( weight > 1 ) {
			weight = 1;
		}
		
		var morph_id = this.morphable.obj3d.morphTargetDictionary[ name ];
		this.morphable.obj3d.morphTargetInfluences[ morph_id ] = weight;
		
		if ( this.butt_animations[ name ] !== undefined ) {
			this.butt_animations[ name ].time = weight * 100;
		}

	};

	this.update_morph_weight = function( delta ) {

		for( var k in this.morph_weights ) {

			var interp = this.morph_weights[k];

			if ( this.request_morph !== undefined && this.request_morph == k ) {
				interp.target = 1;
				if ( this.butt_animations[ k ] !== undefined ) {
					this.butt_animations[ k ].play();
				}
			} else if ( this.request_morph !== undefined ) {
				interp.target = 0;
			}

			if ( !interp.idle ) {
				var morph_id = this.morphable.obj3d.morphTargetDictionary[k];
				this.morphable.obj3d.morphTargetInfluences[ morph_id ] = interp.value;
				if ( interp.value > 0 && this.butt_animations[ k ] !== undefined ) {
					// this.butt_animations[ k ].setEffectiveWeight(interp.value);
					this.butt_animations[ k ].time = interp.value * 100;
				} else if ( interp.value == 0 ) {
					this.butt_animations[ k ].stop();
				}
			}

		}

		this.request_morph = undefined;

	};

	this.update = function( delta ) {

		if ( this.ready === false ) return;

		if ( this.pouf_mixer !== undefined ) {
			this.pouf_mixer.update( delta );
		}

		this.update_morph_weight( delta );
		this.butt_mixer.update( delta );

	};

};
