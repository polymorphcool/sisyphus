/*

 _________ ____  .-. _________/ ____ .-. ____
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'

 libre video game bazaar

 ____________________________________  ?   ____________________________________
                                     (._.)


 This file is part of sysiphus project
 For the latest info, see http://polymorph.cool/

 Copyright (c) 2018 polymorph.cool
 All rights reserved.

 MIT License
DONE
 Permission is hereby granted, free of charge, to any person obtaining a copy of this
 software and associated documentation files (the "Software"), to deal in the Software
 without restriction, including without limitation the rights to use, copy, modify, merge,
 publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED *AS IS*, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.

 ___________________________________( ^3^)_____________________________________

 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)

*/

function RandomTrigger( min, max, listener ) {

	this.second_min = min;
	this.second_max = max;
	this.second_current = -1;
	this.last_duration = 0;
	this.listener = listener;

	this.stop = function() {
		this.second_current = -1;
	};

	this.start = function() {
		this.second_current =
			this.second_min +
			Math.random() * ( this.second_max - this.second_min );
		this.last_duration = this.second_current;
	};

	this.update = function( delta ) {

		if ( this.second_current == -1 ) {
			return;
		}

		this.second_current -= delta;
		if ( this.second_current <= 0 ) {
			if ( this.listener !== undefined ) {
				this.listener.on_random_trigger( this );
			}
			this.start();
		}

	};

};

function TimeManager() {

	this.ready = false;
	this.open_enabled = false;
	this.open_finished = false;
	this.open_inter = undefined;
	this.open_lights = [];

	this.entry_enabled = false;
	this.entry_finished = false;

	this.sit_enabled = false;
	this.sit_finished = false;
	this.sit_inter = undefined;
	this.sitting_speed = 1.2;

	this.morph_enabled = false;
	this.morph_finished = false;
	this.morph_inter = undefined;
	this.morph_name = undefined;
	this.morph_speed_min = 0.05;
	this.morph_speed_max = 0.1;
	this.morph_random_trigger = undefined;

	this.rise_enabled = false;
	this.rise_finished = false;
	this.rising_speed = 0.8;

	this.start_date = undefined;
	this.avatar_entry_pq = undefined;
	this.avatar_active_pq = undefined;

	this.camera_tracking_enabled = true;

	this.do_sync_timestamp = false;

	// utils
	this.camera_tracking_enable = function( enable ) {
		this.camera_tracking_enabled = enable;
		if (
			this.ready &&
			!this.camera_tracking_enabled &&
			head_tracker.active_track_name == "camera"
			) {
			head_tracker.track();
		}
	};

	// initalisation and management
	this.init = function() {

		this.open_lights.push( {
			'obj': ambient_light,
			'color': new THREE.Color( ambient_light.color )
		} );

		this.open_lights.push( {
			'obj': main_light,
			'color': new THREE.Color( main_light.color )
		} );

		this.open_lights.push( {
			'obj': second_light,
			'color': new THREE.Color( second_light.color )
		} );

		for ( var ol in this.open_lights ) {
			this.open_lights[ol]['obj'].color = new THREE.Color( 0x000000 );
		}

		this.open_inter = Interpolation.new_interpolation(
			"opening", 0, 1, Interpolation.sin
		);
		this.sit_inter = Interpolation.new_interpolation(
			"sitting", 0, 1.2, Interpolation.sin
		);
		this.morph_inter = Interpolation.new_interpolation(
			"morphing", 0, 0.001
			// "morphing", 0, 0.1
		);

		var p = new THREE.Vector3(0, -0.005, 0.325);
		var q = new THREE.Quaternion().setFromEuler(
			new THREE.Euler( 0, Math.PI, 0, 'XYZ' )
		);
		var s = new THREE.Vector3(1,1,1);
		this.avatar_entry_matrix = new THREE.Matrix4();
		this.avatar_active_matrix = new THREE.Matrix4();

		this.morph_random_trigger = new RandomTrigger(
			10, 20, this
		);

	};

	this.apply_avatar_pq = function( pq ) {
		avatar.obj3d.position.copy( pq[0] );
		avatar.obj3d.quaternion.copy( pq[1] );
		avatar.obj3d.matrixWorldNeedsUpdate = true;
	};

	this.start = function( delta ) {

		if ( timestamp_data !== undefined ) {
			console.log( "TimeManager.start: timestamp server is ready!" );
			this.do_sync_timestamp = true;
		}

		this.ready = true;
		this.start_opening();

		var q = avatar.obj3d.quaternion.clone();

		this.avatar_active_pq = [
			new THREE.Vector3(0, -0.005, 0.325),
			q.clone()
		];

		var up = new THREE.Vector3(0,1,0);
		up.applyQuaternion( q );
		q = new THREE.Quaternion().setFromEuler(
			new THREE.Euler( Math.PI * 0.5, Math.PI, 0, 'XYZ' )
		);
		this.avatar_entry_pq = [ new THREE.Vector3(0, -0.005, 0.625), q ];

		this.apply_avatar_pq(this.avatar_entry_pq);

	};

	this.sync_timestamp = function( delta ) {

		if ( timestamp_data === undefined ) {
			return;
		}

		this.on_rised();
		this.rise_finished = false;

		this.start_opening();
		this.open_inter.current = this.open_inter.target;
		this.open_inter.idle = false;
		this.open_inter.value = 1;
		this.update_opening( delta );
		this.on_opened();
		this.open_finished = false;

		// this.start_entrying();
		this.entry_enabled = true;
		this.entry_finished = false;
		if ( this.morph_name !== undefined ) {
			pouf.set_influence( this.morph_name, 0 );
			this.morph_name = undefined;
		}
		pouf.visible( true );
		this.on_entryed();
		this.entry_finished = false;

		this.start_sitting();
		this.sit_inter.current = this.sit_inter.target;
		this.sit_inter.idle = false;
		this.sit_inter.value = 1;
		this.update_sitting();
		this.on_sitted();
		this.sit_finished = false;

		this.start_morphing();
		this.morph_inter.idle = false;

	}

	this.update = function( delta ) {

		if ( !this.ready ) { return; }

		if ( this.do_sync_timestamp ) {
			this.sync_timestamp( delta );
			this.do_sync_timestamp = false;
		}

		this.morph_random_trigger.update( delta );

		if ( this.open_finished ) {
			this.open_finished = false;
			this.start_entrying();
		} else if ( this.entry_finished ) {
			this.entry_finished = false;
			this.start_sitting();
		} else if ( this.sit_finished ) {
			this.sit_finished = false;
			this.start_morphing();
		} else if ( this.morph_finished ) {
			this.morph_finished = false;
			this.start_rising();
		} else if ( this.rise_finished ) {
			this.rise_finished = false;
			this.start_entrying();
		}

		if ( this.open_enabled ) {
			this.update_opening( delta );
		} else if ( this.entry_enabled ) {
			this.update_entrying( delta );
		} else if ( this.sit_enabled ) {
			this.update_sitting( delta );
		} else if ( this.morph_enabled ) {
			this.update_morphing( delta );
		} else if ( this.rise_enabled ) {
			this.update_sitting( delta );
		}

	};

	// OPENING

	this.start_opening = function( ) {
		this.open_enabled = true;
		this.open_finished = false;
		this.open_inter.target = 1;
	};

	this.update_opening = function( delta ) {

		if ( !this.open_inter.idle ) {
			for ( var ol in this.open_lights ) {
				var c = new THREE.Color( 0x000000 );
				// var rand = Math.random();
				// if ( rand <  this.open_inter.value ) {
				// 	c.lerp( this.open_lights[ol]['color'], this.open_inter.value );
				// }
				c.lerp( this.open_lights[ol]['color'], this.open_inter.value );
				this.open_lights[ol]['obj'].color = c;
			}
			var prev = this.open_enabled;
			this.open_enabled = this.open_inter.value != 1;
			if ( prev && prev != this.open_enabled ) {
				this.on_opened();
			}
		}

	};

	this.on_opened = function() {
		// pouf.visible( true );
		this.open_enabled = false;
		this.open_finished = true;
	};

	// ENTRY OF THE CUBE

	this.start_entrying = function() {

		this.entry_enabled = true;
		this.entry_finished = false;
		if ( this.morph_name !== undefined ) {
			pouf.set_influence( this.morph_name, 0 );
			this.morph_name = undefined;
		}
		pouf.visible( true );
		pouf.start_falling();
		head_tracker.track( "pouf", undefined, 2 );

	};

	this.update_entrying = function( delta ) {

		if ( pouf.pouf_is_falling ) { return; }

		this.on_entryed();

	};

	this.on_entryed = function() {

		this.entry_enabled = false;
		this.entry_finished = true;
		if ( this.camera_tracking_enabled ) {
			head_tracker.track( "camera" );
		} else {
			head_tracker.track();
		}

	};

	// SITTING SEQUENCE

	this.start_sitting = function() {

		this.apply_avatar_pq( this.avatar_active_pq );
		this.sit_enabled = true;
		this.sit_finished = false;
		this.sit_inter.target = 1;
		this.sit_inter.speed = this.sitting_speed;
		avatar.ik( "mixamorig_LeftFoot", pouf.left_foot, 1 );
		avatar.ik( "mixamorig_RightFoot", pouf.right_foot, 1 );

	};

	this.update_sitting = function( delta ) {

		if ( !this.sit_inter.idle ) {
			avatar.glue_to( "mixamorig_Hips", pouf.butt, this.sit_inter.value );
			if ( this.sit_enabled && this.sit_inter.value == this.sit_inter.target ) {
				this.on_sitted();
			} else if ( this.rise_enabled && this.sit_inter.value == this.sit_inter.target ) {
				this.on_rised();
			}
		}

	};

	this.on_sitted = function() {
		this.sit_enabled = false;
		this.sit_finished = true;
	};

	// MORPHING SEQUENCE

	this.start_morphing = function() {

		this.morph_enabled = true;
		this.morph_inter.current = 0;
		this.morph_inter.target = 1;
		this.start_date = new Date();
		this.morph_random_trigger.start();

		if ( timestamp_data !== undefined ) {

			this.morph_name = timestamp_data['shape'];
			this.morph_inter.speed = timestamp_data['speed'];

		} else {

			var all_keys = Object.keys(pouf.butt_animations);
			this.morph_name = all_keys[ Math.ceil( Math.random() * all_keys.length ) ];

		}

	};

	this.update_morphing = function( delta ) {

		if ( !this.morph_inter.idle || timestamp_data !== undefined ) {

			pouf.set_influence( this.morph_name, this.morph_inter.value );

			if ( timestamp_data !== undefined ) {
				if ( timestamp_data['shape'] == this.morph_name ) {
					// slowly synching heads
					var c = timestamp_data['delta'] / timestamp_data['max'];
					if ( c > 1 ) {
						c = 1;
					}
					if ( this.morph_inter.current < c ) {
						this.morph_inter.current += ( c - this.morph_inter.current ) * 3 * delta;
					}
				} else {
					
					// console.log( timestamp_data['shape'] );
					this.morph_inter.current = this.morph_inter.target;
					this.on_morphed();
					
					// reseting camera
					camera_position_transition_current = 0;
					current_camera_position = new THREE.Vector3( camera.position.x, camera.position.y, camera.position.z );
					target_camera_position = default_camera_position;
					current_camera_target = controls.target.clone();
					
				}
			} else if ( this.morph_inter.value == 1 ) {
				this.on_morphed();
			}

		}

	};

	this.on_morphed = function() {

		var end_date = new Date();
		var elapsed = end_date - this.start_date;

		if ( !gallery_server_enabled ) {
			gallery_data.push(
				this.morph_name +'|'+
				Basics.nice_date() +'|' +
				Basics.nice_time( elapsed )
			);
			update_gallery();
		} else {
			gallery_request(true);
		}

		this.morph_enabled = false;
		this.morph_finished = true;

		this.morph_random_trigger.stop();

	};

	// RISING

	this.start_rising = function() {

		this.rise_enabled = true;
		this.rise_finished = false;
		this.sit_inter.target = 0;
		this.sit_inter.speed = this.rising_speed;
		avatar.set_animation_name( "idle" );

	};

	this.on_rised = function() {

		this.rise_enabled = false;
		this.rise_finished = true;
		avatar.ik( "mixamorig_Hips", undefined, 0 );
		avatar.ik( "mixamorig_LeftFoot", undefined, 0 );
		avatar.ik( "mixamorig_RightFoot", undefined, 0 );

		this.apply_avatar_pq(this.avatar_entry_pq);

	};

	this.on_random_trigger = function( trigger ) {

		if ( this.morph_random_trigger == trigger ) {

			// var r = Math.random();
			var anim = config_sysiphus_avatar.name_animations[
				Math.floor(
				Math.random() *
				config_sysiphus_avatar.name_animations.length
			)];

			var on_loop = anim;

			if ( anim == "baille" ) {
				on_loop = "idle";
			}

			avatar.set_animation_name( anim, on_loop );

		}

	};

};
