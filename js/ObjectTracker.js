/*

 _________ ____  .-. _________/ ____ .-. ____
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'

 libre video game bazaar

 ____________________________________  ?   ____________________________________
                                     (._.)


 This file is part of sysiphus project
 For the latest info, see http://polymorph.cool/

 Copyright (c) 2018 polymorph.cool
 All rights reserved.

 MIT License

 Permission is hereby granted, free of charge, to any person obtaining a copy of this
 software and associated documentation files (the "Software"), to deal in the Software
 without restriction, including without limitation the rights to use, copy, modify, merge,
 publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED *AS IS*, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.

 ___________________________________( ^3^)_____________________________________

 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)

*/


// RECOGNISE TYPES OF TARGETS:
// * vec3
// * obj3d -> will use

function TrackerTrack() {

	this.name = undefined;
	this.type = undefined;
	this.obj = undefined;
	this.weight_inter = undefined;
	this.ready = false;

	// initalisation and management
	this.init = function( name, obj ) {

		if ( THREE.Object3D.prototype.isPrototypeOf( obj ) ) {
			this.type = THREE.Object3D;
		} else if ( THREE.Vector3.prototype.isPrototypeOf( obj ) ) {
			this.type = THREE.Vector3;
		} else {
			console.log( "Tracker.add, unrecognised class!", obj );
			return undefined;
		}
		this.name = name;
		this.obj = obj;
		this.weight_inter = Interpolation.new_interpolation(
			"_ttrack_"+this.name, 0, 0.5 );
		this.ready = true;
		return this;

	};

	this.enable = function( e ) {
		if ( !this.ready ) {
			return;
		}
		if ( e == true ) {
			this.weight_inter.target = 1;
		} else {
			this.weight_inter.target = 0;
		}
	};

	this.lerp = function( vec ) {

		if (
			!this.ready ||
			this.weight_inter.value == 0
			) {
				return;
		}

		if ( this.type === THREE.Vector3 ) {
			vec.lerp( this.obj, this.weight_inter.value );
		} else if ( this.type === THREE.Object3D ) {
			var v = new THREE.Vector3();
			this.obj.getWorldPosition(v);
			vec.lerp( v, this.weight_inter.value );
		}

	};

};

function Tracker() {

	this.name = undefined;
	this.general_inter = undefined;
	this.all_tracks = undefined;
	this.active_track_name = "";
	this.duration = undefined;

	this.influences = undefined;

	this.obj3d = undefined;
	this.scene = undefined;

	this.init = function( name ) {

		this.name = name;
		this.general_inter = Interpolation.new_interpolation(
			"_tracker_"+this.name, 0, 2,
			Interpolation.sin );
		this.all_tracks = {};
		this.influences = [];
		this.scene = scene;
		this.obj3d = new THREE.Object3D();
		this.scene.add( this.obj3d );
		// this.obj3d.add( Basics.debug_axis() );

	};

	this.add_track = function( name, v ) {
		this.all_tracks[ name ] = new TrackerTrack().init( name, v );
	};

	this.add_influence = function(
		obj, bone_name, weight ) {
		this.influences.push( {
			'obj': obj,
			'bone_name': bone_name,
			'weight': weight
		} );
	}

	this.track = function( name, duration, speed ) {

		this.active_track_name = name;

		for ( var n in this.all_tracks ) {
			var t = this.all_tracks[ n ];
			if ( n == name ) {
				t.enable( true );
			} else {
				t.enable( false );
			}
		}

		if ( name === undefined ) {
			this.general_inter.target = 0;
		} else {
			this.general_inter.target = 1;
		}

		this.duration = duration;

		if ( speed !== undefined ) {
			this.general_inter.speed = speed;
		}

	};

	this.update = function( delta ) {

		if ( this.duration !== undefined ) {
			this.duration -= delta;
			if ( this.duration < 0 ) {
				this.track();
				this.duration = undefined;
			}
		}

		// general_inter can be to 1,
		// but track object might have changed
		// therefore, soft lock instead of testing this.general_inter.idle
		if ( this.general_inter.value == 0 ) {
			return;
		}

		var p = this.obj3d.position;
		for ( var n in this.all_tracks ) {
			var t = this.all_tracks[ n ];
			if ( !t.ready ) {
				continue;
			}
			t.lerp( p );
		}

		this.obj3d.position.copy( p );
		this.obj3d.updateMatrix();

		var weight = this.general_inter.value;
		
		for( var i = 0; i < this.influences.length; ++i ) {
			var influ = this.influences[i];
			if ( weight == 0 ) {
				influ['obj'].track_to(
						influ['bone_name'],
						undefined );
			} else {
				influ['obj'].track_to(
						influ['bone_name'],
						this.obj3d,
						weight * influ['weight']
					);
			}
		}

	};

};
