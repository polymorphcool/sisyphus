# LICENSES #

Sisysphus is using different licenses, please read carefully before using the sources, assets or any other thing in this repository.

## assets ##

By assets we mean any file that is not a script or code:

* 3D files ( .blend, .dae, .json ),
* image files ( .png, .jpg, etc.),
* sound files ( .wav, .aiff, .mp3, etc.)

All assets are under [Creative Commons - Attribution-NonCommercial-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode).

Please refer to [Simone Niquille](http://technofle.sh) and [François Zajéga](http://polymorph.cool) as co-authors.

## fonts ##

* WebHostingHub Glyphs, by 	Web Hosting Hub, https://www.webhostinghub.com/glyphs/
* Lincoln/MITRE, by David Bennewith, http://colophon.info/
* Space Mono, by Colophon Foundry, https://www.typewolf.com/site-of-the-day/fonts/space-mono

## code ##

By code we mean any javascript, html, css, python & sh scripts, excluding .json files.

* threejs is released under MIT license, https://github.com/mrdoob/three.js/
* range-slider.js, by André Ruffert, is released under MIT license, https://github.com/andreruffert/rangeslider.js

All code developped by Simone & François is under [MIT License](https://opensource.org/licenses/MIT).

MIT License

Copyright (c) 2018 polymorph.cool and technofle.sh

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
