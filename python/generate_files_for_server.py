import os, shutil

model_folder = '../models/generated'
model_needle = '_icon.png'

log_folder = '../server/generated'

files = os.listdir( model_folder )
files.sort()

if not os.path.exists(log_folder):
	os.makedirs(log_folder)

for f in files:
	
	if f[-len(model_needle):] == model_needle:
		log = open( os.path.join( log_folder, f[:-len(model_needle)] ), 'w' )
		log.close()