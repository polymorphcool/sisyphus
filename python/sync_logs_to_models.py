import os, shutil

overwrite_icon = False
overwrite_morph = False
overwrite_diff = False
overwrite_legs = False

inventory = '../server/sisyphus.inventory'
models = '../../private/models/generator/generated'
model_needles = [ '_icon.png', '_morph.json', '_diffuse.png', '_legs.json' ]
deploy = '../models/generated'
html_path = 'models/generated'
compress = False

logs = []

time_names = [ 'day', 'hour', 'minute', 'second' ]
time_mult = [ 86400, 3600, 60, 1 ]

# getting the generation logs and preparing the structure

files = os.listdir( inventory )
files.sort()

for f in files:
	
	blocks = f.split( 'T' )
	blocks = blocks[1].split( ':' )
	
	finish = 0
	finish_readable = ''
	for i in range( 0, 3 ):
		val = int( float( blocks[i] ) )
		finish += val * time_mult[i+1]
		if i > 0:
			finish_readable += ':'
		if val < 10:
			finish_readable += '0'
		finish_readable += str(val)
	
	log = open( os.path.join( inventory, f ) )
	data = log.read().strip()
	blocks = data.split( '|' )
	date = blocks[1]
	duration = 0
	duration_readable = ''
	blocks = blocks[2].split('-')
	for i in range( 0, 4 ):
		val = int( blocks[i] )
		duration += val * time_mult[i]
		if i > 0:
			duration_readable += ', '
		if val < 10:
			duration_readable += '0'
		duration_readable += blocks[i] + ' ' + time_names[i]
		if val > 1:
			duration_readable += 's'
			
	logs.append(
		{
			'icon': '',
			'morph': '',
			'diffuse': '',
			'legs': '',
			'name': f,
			'log' : os.path.join( inventory, f ),
			'date': date,
			'finish_readable': finish_readable,
			'finish': finish,
			'duration_readable': duration_readable,
			'duration': duration,
			'blend': ''
		}
	)

# synching generated files with logs

directories = os.listdir( models )
directories.sort()

dcount = 0
for i in range( 0, len( directories ) ):
	
	if dcount >= len( logs ):
		print( "too many models!" )
		break
	
	if directories[i][-5:] != '_done':
		continue
	
	files = os.listdir( os.path.join( models, directories[i] ) )
	found = 0
	icon = ''
	morph = ''
	diff = ''
	legs = ''
	blend = ''
	for f in files:
		#target = os.path.basename( f )
		if f[-len(model_needles[0]):] == model_needles[0]:
			icon = os.path.join( models, directories[i], f )
			found += 1
		if f[-len(model_needles[1]):] == model_needles[1]:
			morph = os.path.join( models, directories[i], f )
			found += 1
		if f[-len(model_needles[2]):] == model_needles[2]:
			diff = os.path.join( models, directories[i], f )
			found += 1
		if f[-len(model_needles[3]):] == model_needles[3]:
			legs = os.path.join( models, directories[i], f )
			found += 1
		if f[-6:] == '.blend':
			blend = f
	
	if found != len( model_needles ):
		print( "missing mandatory files in " + directories[i] )
		continue
	
	log = logs[dcount]
	log['icon'] = icon
	log['morph'] = morph
	log['diff'] = diff
	log['legs'] = legs
	log['blend'] = blend
	
	dcount += 1

# moving resources at the right location and generating json


if not os.path.isdir(deploy):
	os.makedirs(deploy)

tab1 = '\n\t'
tab2 = '\n\t\t'
tab3 = '\n\t\t\t'
if compress:
	tab2 = ''
	tab3 = ''

json = open( os.path.join( deploy, 'sisyphus_cubes.json' ), 'w' )
json.write( '{' + tab1 + '"data":[' )

model_count = 0
for l in logs:
	
	if l['icon'] == '' or l['morph'] == '' or l['diff'] == '' or l['legs'] == '':
		break
	
	newpath_icon = os.path.join( deploy, l['name'] + model_needles[0] )
	newpath_morph = os.path.join( deploy, l['name'] + model_needles[1] )
	newpath_diff = os.path.join( deploy, l['name'] + model_needles[2] )
	newpath_legs = os.path.join( deploy, l['name'] + model_needles[3] )
	
	print( "pushing model " + l['name'] + " " + str(model_count) + "/" + str(len(logs) - 1) )
	
	if overwrite_icon or not os.path.isfile(newpath_icon):
		shutil.copyfile( l['icon'], newpath_icon )
	if overwrite_morph or not os.path.isfile(newpath_morph):
		shutil.copyfile( l['morph'], newpath_morph )
	if overwrite_diff or not os.path.isfile(newpath_diff):
		shutil.copyfile( l['diff'], newpath_diff )
	if overwrite_legs or not os.path.isfile(newpath_legs):
		shutil.copyfile( l['legs'], newpath_legs )
	
	if model_count > 0:
		json.write( ',' )
	json.write( tab2 + '{' )
	json.write( tab3+'"id": ' + str( model_count ) + ',' )
	json.write( tab3+'"name": "' + l['name'] + '",' )
	json.write( tab3+'"icon": "' + os.path.join( html_path, l['name'] + model_needles[0] ) + '",' )
	json.write( tab3+'"morph": "' + os.path.join( html_path, l['name'] + model_needles[1] ) + '",' )
	json.write( tab3+'"diff": "' + os.path.join( html_path, l['name'] + model_needles[2] ) + '",' )
	json.write( tab3+'"legs": "' + os.path.join( html_path, l['name'] + model_needles[3] ) + '",' )
	json.write( tab3+'"date": "' + l['date'] + '",' )
	json.write( tab3+'"finish_r": "' + l['finish_readable'] + '",' )
	json.write( tab3+'"duration_r": "' + l['duration_readable'] + '",' )
	json.write( tab3+'"finish": ' + str(l['finish']) + ',' )
	json.write( tab3+'"duration": ' + str(l['duration']) + ',' )
	json.write( tab3+'"blend": "' + l['blend'] + '"' )
	json.write( tab2 +'}' )
	json.flush()
	
	model_count += 1
	
json.write( tab1 + ']\n}' )

'''
for l in logs:
	print( l )
'''