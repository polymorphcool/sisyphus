import bpy
import json

json_path = bpy.context.blend_data.filepath[:bpy.context.blend_data.filepath.rfind('/')+1] + 'tracker_timeline.json'

f = open(json_path)
data = json.load(f)

for key in data:
	obj = bpy.data.objects[ key ]
	if obj != None:
		keyframes = data[key]
		for kf in keyframes:
			print( kf )
			try:
				bpy.context.scene.frame_current = kf['frame']
				obj.location = kf['position']
				obj.keyframe_insert(data_path='location', frame=(kf['frame']))
				obj.rotation_euler = kf['eulers']
				obj.keyframe_insert(data_path='rotation_euler', frame=(kf['frame']))
				print( bpy.context.scene.frame_current )
			except:
				break
