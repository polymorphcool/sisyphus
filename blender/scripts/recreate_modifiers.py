import bpy

obj_names = [ 'cube', 'butt_physic' ]

# regeneration of modifiers
for n in obj_names:
	obj = bpy.data.objects[ n ]
	obj.modifiers.clear()
	obj.modifiers.new( name = 'gen_softbody', type = 'SOFT_BODY' )
	obj.modifiers.new( name = 'gen_subsurface', type = 'SUBSURF' )
	obj.modifiers.new( name = 'gen_collision', type = 'COLLISION' )
bpy.data.objects['butt_physic'].modifiers.new( name = 'gen_wireframe', type = 'WIREFRAME' )

# interesting params
bpy.data.objects['cube'].modifiers['gen_softbody'].settings.friction = 4.0

#bpy.data.objects['cube'].modifiers['gen_softbody'].settings.goal_max = 0.5999999642372131
#bpy.data.objects['cube'].modifiers['gen_softbody'].settings.goal_friction = 5.899998664855957
#bpy.data.objects['cube'].modifiers['gen_softbody'].settings.goal_min = 0.0
#bpy.data.objects['cube'].modifiers['gen_softbody'].settings.goal_spring = 0.19999998807907104
bpy.data.objects['cube'].modifiers['gen_softbody'].settings.goal_default = 0.3999999761581421
bpy.data.objects['cube'].modifiers['gen_softbody'].settings.goal_max = 0.83
bpy.data.objects['cube'].modifiers['gen_softbody'].settings.goal_friction = 0.899998664855957
bpy.data.objects['cube'].modifiers['gen_softbody'].settings.goal_min = 0.2
bpy.data.objects['cube'].modifiers['gen_softbody'].settings.goal_spring = 0.65

#bpy.data.objects['cube'].modifiers['gen_softbody'].settings.pull = 0.4000000059604645
#bpy.data.objects['cube'].modifiers['gen_softbody'].settings.plastic = 5
bpy.data.objects['cube'].modifiers['gen_softbody'].settings.mass = 0.10000000149011612
bpy.data.objects['cube'].modifiers['gen_softbody'].settings.plastic = 0
bpy.data.objects['cube'].modifiers['gen_softbody'].settings.pull = 0.6
bpy.data.objects['cube'].modifiers['gen_softbody'].settings.push = 0.7999999523162842
bpy.data.objects['cube'].modifiers['gen_softbody'].settings.shear = 0.20000000298023224
bpy.data.objects['cube'].modifiers['gen_softbody'].settings.speed = 1.0
bpy.data.objects['cube'].modifiers['gen_softbody'].settings.bend = 3.3999996185302734
bpy.data.objects['cube'].modifiers['gen_softbody'].settings.damping = 11.599998474121094

# less interesting params
bpy.data.objects['cube'].modifiers['gen_softbody'].settings.aero = 0
bpy.data.objects['cube'].modifiers['gen_softbody'].settings.aerodynamics_type = 'SIMPLE'
bpy.data.objects['cube'].modifiers['gen_softbody'].settings.ball_damp = 0.0010000000474974513
bpy.data.objects['cube'].modifiers['gen_softbody'].settings.ball_size = 0.6000000238418579
bpy.data.objects['cube'].modifiers['gen_softbody'].settings.ball_stiff = 1.0
bpy.data.objects['cube'].modifiers['gen_softbody'].settings.choke = 3
bpy.data.objects['cube'].modifiers['gen_softbody'].settings.collision_group = None
bpy.data.objects['cube'].modifiers['gen_softbody'].settings.collision_type = 'AVERAGE'
bpy.data.objects['cube'].modifiers['gen_softbody'].settings.error_threshold = 0.10000000149011612
bpy.data.objects['cube'].modifiers['gen_softbody'].settings.fuzzy = 0
bpy.data.objects['cube'].modifiers['gen_softbody'].settings.gravity = 9.800000190734863
bpy.data.objects['cube'].modifiers['gen_softbody'].settings.spring_length = 0
bpy.data.objects['cube'].modifiers['gen_softbody'].settings.step_max = 300
bpy.data.objects['cube'].modifiers['gen_softbody'].settings.step_min = 10
bpy.data.objects['cube'].modifiers['gen_softbody'].settings.use_auto_step = True
bpy.data.objects['cube'].modifiers['gen_softbody'].settings.use_diagnose = False
bpy.data.objects['cube'].modifiers['gen_softbody'].settings.use_edge_collision = False
bpy.data.objects['cube'].modifiers['gen_softbody'].settings.use_edges = True
bpy.data.objects['cube'].modifiers['gen_softbody'].settings.use_estimate_matrix = False
bpy.data.objects['cube'].modifiers['gen_softbody'].settings.use_face_collision = False
bpy.data.objects['cube'].modifiers['gen_softbody'].settings.use_goal = True
bpy.data.objects['cube'].modifiers['gen_softbody'].settings.use_self_collision = True
bpy.data.objects['cube'].modifiers['gen_softbody'].settings.use_stiff_quads = True
bpy.data.objects['cube'].modifiers['gen_softbody'].settings.vertex_group_goal = 'rigids'
bpy.data.objects['cube'].modifiers['gen_softbody'].settings.vertex_group_mass = ''
bpy.data.objects['cube'].modifiers['gen_softbody'].settings.vertex_group_spring = 'rigids'
bpy.data.objects['cube'].modifiers['gen_softbody'].point_cache.compression = 'NO'
bpy.data.objects['cube'].modifiers['gen_softbody'].point_cache.filepath = ''
bpy.data.objects['cube'].modifiers['gen_softbody'].point_cache.frame_end = 100
bpy.data.objects['cube'].modifiers['gen_softbody'].point_cache.frame_start = 1
bpy.data.objects['cube'].modifiers['gen_softbody'].point_cache.frame_step = 1
bpy.data.objects['cube'].modifiers['gen_softbody'].point_cache.index = -1
bpy.data.objects['cube'].modifiers['gen_softbody'].point_cache.name = ''
bpy.data.objects['cube'].modifiers['gen_softbody'].point_cache.use_disk_cache = False
bpy.data.objects['cube'].modifiers['gen_softbody'].point_cache.use_external = False
bpy.data.objects['cube'].modifiers['gen_softbody'].point_cache.use_library_path = True

bpy.data.objects['butt_physic'].modifiers['gen_softbody'].settings.aero = 0
bpy.data.objects['butt_physic'].modifiers['gen_softbody'].settings.aerodynamics_type = 'SIMPLE'
bpy.data.objects['butt_physic'].modifiers['gen_softbody'].settings.ball_damp = 0.5
bpy.data.objects['butt_physic'].modifiers['gen_softbody'].settings.ball_size = 0.49000000953674316
bpy.data.objects['butt_physic'].modifiers['gen_softbody'].settings.ball_stiff = 1.0
bpy.data.objects['butt_physic'].modifiers['gen_softbody'].settings.bend = 10.0
bpy.data.objects['butt_physic'].modifiers['gen_softbody'].settings.choke = 3
bpy.data.objects['butt_physic'].modifiers['gen_softbody'].settings.collision_group = None
bpy.data.objects['butt_physic'].modifiers['gen_softbody'].settings.collision_type = 'AVERAGE'
bpy.data.objects['butt_physic'].modifiers['gen_softbody'].settings.damping = 5.3000006675720215
bpy.data.objects['butt_physic'].modifiers['gen_softbody'].settings.error_threshold = 0.10000000149011612
bpy.data.objects['butt_physic'].modifiers['gen_softbody'].settings.friction = 4.0
bpy.data.objects['butt_physic'].modifiers['gen_softbody'].settings.fuzzy = 0
bpy.data.objects['butt_physic'].modifiers['gen_softbody'].settings.goal_default = 0.699999988079071
bpy.data.objects['butt_physic'].modifiers['gen_softbody'].settings.goal_friction = 0.0
bpy.data.objects['butt_physic'].modifiers['gen_softbody'].settings.goal_max = 1.0
bpy.data.objects['butt_physic'].modifiers['gen_softbody'].settings.goal_min = 0.0
bpy.data.objects['butt_physic'].modifiers['gen_softbody'].settings.goal_spring = 0.5
bpy.data.objects['butt_physic'].modifiers['gen_softbody'].settings.gravity = 9.800000190734863
bpy.data.objects['butt_physic'].modifiers['gen_softbody'].settings.mass = 9.0
bpy.data.objects['butt_physic'].modifiers['gen_softbody'].settings.plastic = 0
bpy.data.objects['butt_physic'].modifiers['gen_softbody'].settings.pull = 0.5
bpy.data.objects['butt_physic'].modifiers['gen_softbody'].settings.push = 0.5
bpy.data.objects['butt_physic'].modifiers['gen_softbody'].settings.shear = 1.0
bpy.data.objects['butt_physic'].modifiers['gen_softbody'].settings.speed = 1.0
bpy.data.objects['butt_physic'].modifiers['gen_softbody'].settings.spring_length = 0
bpy.data.objects['butt_physic'].modifiers['gen_softbody'].settings.step_max = 300
bpy.data.objects['butt_physic'].modifiers['gen_softbody'].settings.step_min = 10
bpy.data.objects['butt_physic'].modifiers['gen_softbody'].settings.use_auto_step = True
bpy.data.objects['butt_physic'].modifiers['gen_softbody'].settings.use_diagnose = False
bpy.data.objects['butt_physic'].modifiers['gen_softbody'].settings.use_edge_collision = False
bpy.data.objects['butt_physic'].modifiers['gen_softbody'].settings.use_edges = True
bpy.data.objects['butt_physic'].modifiers['gen_softbody'].settings.use_estimate_matrix = False
bpy.data.objects['butt_physic'].modifiers['gen_softbody'].settings.use_face_collision = False
bpy.data.objects['butt_physic'].modifiers['gen_softbody'].settings.use_goal = False
bpy.data.objects['butt_physic'].modifiers['gen_softbody'].settings.use_self_collision = False
bpy.data.objects['butt_physic'].modifiers['gen_softbody'].settings.use_stiff_quads = True
bpy.data.objects['butt_physic'].modifiers['gen_softbody'].settings.vertex_group_goal = ''
bpy.data.objects['butt_physic'].modifiers['gen_softbody'].settings.vertex_group_mass = 'weights'
bpy.data.objects['butt_physic'].modifiers['gen_softbody'].settings.vertex_group_spring = ''
bpy.data.objects['butt_physic'].modifiers['gen_softbody'].point_cache.compression = 'NO'
bpy.data.objects['butt_physic'].modifiers['gen_softbody'].point_cache.filepath = ''
bpy.data.objects['butt_physic'].modifiers['gen_softbody'].point_cache.frame_end = 100
bpy.data.objects['butt_physic'].modifiers['gen_softbody'].point_cache.frame_start = 1
bpy.data.objects['butt_physic'].modifiers['gen_softbody'].point_cache.frame_step = 1
bpy.data.objects['butt_physic'].modifiers['gen_softbody'].point_cache.index = -1
bpy.data.objects['butt_physic'].modifiers['gen_softbody'].point_cache.name = ''
bpy.data.objects['butt_physic'].modifiers['gen_softbody'].point_cache.use_disk_cache = False
bpy.data.objects['butt_physic'].modifiers['gen_softbody'].point_cache.use_external = False
bpy.data.objects['butt_physic'].modifiers['gen_softbody'].point_cache.use_library_path = True

bpy.context.scene.update()

bpy.context.scene.frame_current = 1

bpy.ops.screen.animation_play()
