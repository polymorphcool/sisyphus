import os,re,subprocess
from shutil import copyfile

hotfolder = '/home/frankiezafe/projects/sisyphus/private/models/generator/generated'
folder_suffix_flag = '_done'
needle = '_icon.png'

def launch_copy( path ):
	files = os.listdir( path )
	files.sort()
	oktogo = False
	for f in files:
		if f[len(f)-len(needle):] == needle:
			copyfile( os.path.join(path,f), os.path.join(gpath,f))
			break

gpath = hotfolder + '/gallery'
if not os.path.isdir(gpath):
	os.makedirs(gpath)

folders = sorted([os.path.join(hotfolder, o) for o in os.listdir(hotfolder) 
	if os.path.isdir(os.path.join(hotfolder,o))])

for f in folders:
	# double check
	if os.path.isdir(f) is True:
		if f[len(f)-len(folder_suffix_flag):] == folder_suffix_flag:
			launch_copy( f )
		#else:
		#	os.rename( f, f[:-len(folder_suffix_flag)] )

day = ''
html = open( os.path.join(gpath,'index.html'), 'w')
html.write( '<html><head><title>Sisyphus gallery</title><style>html,body,document{ margin:0; padding: 0; font-family: courier; font-size:12px; background: #222; color: #fff; } h1,h2{clear:both; margin: 5px 0 0 5px; padding: 0; } div{float:left; margin: 5px 0 0 5px;  border: 1px dotted #aaa;padding: 5px;text-align: center;} div:hover{ font-weight:bold; border-color: #fff; } span{ height: 15px; overflow:hidden; display: block; }</style></head><body>' )
html.write( '<h1>Sisyphus cubes gallery [raw output]</h1>' )

# group by dates
days = {}

for f in os.listdir(gpath):
	if f[len(f)-len(needle):] == needle:
		fday = f[14:-18]
		if not fday in days:
			days[fday] = []
		days[fday].append( f )

keys = days.keys()
keys.sort()

for d in keys:
	html.write( '<h2>' + d + '</h2>' )
	days[d].sort()
	for f in days[d]:
		html.write( '<div><img src="' + f + '"/><br><span>' + f[14:-len(needle)] + '</span></div>' )
		html.flush()
html.write( '</body>' )
html.close()