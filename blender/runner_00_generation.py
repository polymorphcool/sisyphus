import os,re,subprocess
import time

RUN_GENERATION = True
RUN_RENDER = True
RUN_LEGS = False

blend_exec = '/opt/blender-2.79b-linux-glibc219-x86_64/./blender'
template_path = os.path.abspath( 'generate.blend' )
generation_script = os.path.abspath( 'scripts/main.py' )
max_run = 1
current_run = 0

previous_run_time = time.time()
total_run_time = 0

def launch_generation():
	global current_run
	global previous_run_time
	print( "\t####################################################")
	print( "\t############# starting run " + str(current_run) + '/' + str(max_run) + " #############" )
	print( "\t####################################################")
	p1 = subprocess.Popen( [ blend_exec, template_path, '-P',  generation_script ] )
	p1.wait()
	total_run_time = time.time() - previous_run_time
	previous_run_time = time.time()
	print( "\t####################################################")
	print( '\ttotal run time: ' + time.strftime("%H:%M:%S", time.gmtime(total_run_time)) )
	print( "\t####################################################")
	current_run += 1

if RUN_GENERATION:
	for i in range( 0, max_run ):
		launch_generation()

'''
# usefull to modify the frame gaps specified in 'render-file.py'
if modify_render_files:
	p1 = subprocess.Popen( [ 'python', 'scripts/render_file_modifier.py' ] )
	p1.wait()
'''

if RUN_RENDER:
	p1 = subprocess.Popen( [ 'python', 'runner_01_render.py' ] )
	p1.wait()

if RUN_LEGS:
	p1 = subprocess.Popen( [ 'python', 'runner_05_legs.py' ] )
	p1.wait()