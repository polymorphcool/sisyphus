import os, re, subprocess, colorsys
from time import gmtime, strftime
from PIL import Image,ImageFilter

hotfolder = '/home/frankiezafe/projects/sisyphus/private/models/generator/generated'
needle = '_diffuse.png'

blend_exec = '/opt/blender-2.79-linux-glibc219-x86_64/./blender'
script_path = '/home/frankiezafe/projects/sisyphus/private/models/generator/weight_baking.py'

def boost_contrast( input_path, output_path = None ):
	im = Image.open( input_path )
	ld = im.load()
	width, height = im.size
	for y in range(height):
		for x in range(width):
			r,g,b = ld[x,y]
			if r + g + b == 0:
				continue
			higher = 0
			value = r
			if g > value:
				higher = 1
				value = g
			if b > value:
				higher = 2
				value = b
			mult = 255.0 / ld[x,y][higher]
			ld[x,y] = ( int(r*mult), int(g*mult), int(b*mult) )
			'''
			h,s,v = colorsys.rgb_to_hsv(r/255., g/255., b/255.)
			h = (h + -90.0/360.0) % 1.0
			s = s**0.65
			r,g,b = colorsys.hsv_to_rgb(h, s, 1)
			ld[x,y] = (int( v * 255 ),int( v * 255 ),int( v * 255 ))
			ld[x,y] = (int( s * 255 ),int( s * 255 ),int( s * 255 ))
			ld[x,y] = (int(r * 255.9999), int(g * 255.9999), int(b * 255.9999))
			'''
	im = im.filter( ImageFilter.BLUR )
	out = output_path
	if out == None:
		out = input_path + '.png'
	im.save( out,"PNG" )


folders = sorted([os.path.join(hotfolder, o) for o in os.listdir(hotfolder) 
	if os.path.isdir(os.path.join(hotfolder,o))])

for d in folders:
	if os.path.isdir(d) is True:
		files = [f for f in os.listdir(d)]
		needle_found = False
		blend_file = None
		for f in files:
			#print( f[-len(needle):] )
			if f[-len(needle):] == needle:
				needle_found = True
			elif f[-6:] == '.blend':
				blend_file = f
		if not needle_found and blend_file is not None:
			print( "processing " + d + ' > ' + blend_file )
			p1 = subprocess.Popen( [ blend_exec, os.path.join(d,blend_file), '-P', script_path ] )
			p1.wait()
		# seeking diffuse
		diffuse = None
		files = [f for f in os.listdir(d)]
		for f in files:
			if f[-len(needle):] == needle:
				diffuse = f
				break
		if diffuse is not None:
			absp = os.path.join(d,diffuse)
			boost_contrast( absp, absp ) # overwrite
		print( "folder " + d + " finished" )