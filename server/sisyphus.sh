#!/bin/bash

killall node

cd /var/www/scripts/
node http_server.js & # port 3000

cd /var/www/scripts/
node timestamp.js & # port 3001

cd /var/www/scripts/
node gallery.js & # port 3002