#!/bin/bash

cp /var/www/git/nodejs/timestamp.js /var/www/scripts/
cp /var/www/git/nodejs/gallery.js /var/www/scripts/

# creation of required folders
mkdir /var/www/scripts/sisyphus.inventory
mkdir /var/www/scripts/sisyphus.persistent

/etc/init.d/./sisyphus.sh
